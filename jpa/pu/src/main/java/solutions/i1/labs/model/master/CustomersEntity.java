package solutions.i1.labs.model.master;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by cola on 2017/03/23.
 */
@NamedQueries({
        @NamedQuery(name = "CustomersEntity.findAll", query = "SELECT c from CustomersEntity c ORDER BY c.customercode DESC")
})
@Entity
@Table(name = "CUSTOMERS", schema = "MASTER", catalog = "")
public class CustomersEntity {
    private long customerPk;
    private Timestamp lasttouched;
    private String lasttoucheduser;
    private short activestatus;
    private long businessentityPk;
    private String edinum;
    private String customercode;
    private String parentaccnum;
    private String customername;
    private String registeredname;
    private String pharmaregnum;
    private String pharmaexpireydate;
    private String medpagenum;
    private String vatnum;
    private BigDecimal creditlimit;
    private BigDecimal insuredcreditlimit;
    private String insuredcreditlimitcurrency;
    private Date monthenddate;
    private String division;
    private String route;
    private String insurancenum;
    private Date policyexpirydate;
    private String distributorcode;
    private Timestamp docdatetime;
    private String statuscode;
    private String statusdescr;
    private String marketsegment;
    private Timestamp customerlastupd;
    private Short stmtday;
    private Short viewsitestatisticalforecast;
    private Short forecastwindow;
    private Short forecastmandatory;

    @Id
    @Column(name = "CUSTOMER_PK", nullable = false)
    public long getCustomerPk() {
        return customerPk;
    }

    public void setCustomerPk(long customerPk) {
        this.customerPk = customerPk;
    }

    @Basic
    @Column(name = "LASTTOUCHED", nullable = false)
    public Timestamp getLasttouched() {
        return lasttouched;
    }

    public void setLasttouched(Timestamp lasttouched) {
        this.lasttouched = lasttouched;
    }

    @Basic
    @Column(name = "LASTTOUCHEDUSER", nullable = false, length = 30)
    public String getLasttoucheduser() {
        return lasttoucheduser;
    }

    public void setLasttoucheduser(String lasttoucheduser) {
        this.lasttoucheduser = lasttoucheduser;
    }

    @Basic
    @Column(name = "ACTIVESTATUS", nullable = false)
    public short getActivestatus() {
        return activestatus;
    }

    public void setActivestatus(short activestatus) {
        this.activestatus = activestatus;
    }

    @Basic
    @Column(name = "BUSINESSENTITY_PK", nullable = false)
    public long getBusinessentityPk() {
        return businessentityPk;
    }

    public void setBusinessentityPk(long businessentityPk) {
        this.businessentityPk = businessentityPk;
    }

    @Basic
    @Column(name = "EDINUM", nullable = true, length = 12)
    public String getEdinum() {
        return edinum;
    }

    public void setEdinum(String edinum) {
        this.edinum = edinum;
    }

    @Basic
    @Column(name = "CUSTOMERCODE", nullable = true, length = 50)
    public String getCustomercode() {
        return customercode;
    }

    public void setCustomercode(String customercode) {
        this.customercode = customercode;
    }

    @Basic
    @Column(name = "PARENTACCNUM", nullable = true, length = 50)
    public String getParentaccnum() {
        return parentaccnum;
    }

    public void setParentaccnum(String parentaccnum) {
        this.parentaccnum = parentaccnum;
    }

    @Basic
    @Column(name = "CUSTOMERNAME", nullable = true, length = 50)
    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    @Basic
    @Column(name = "REGISTEREDNAME", nullable = true, length = 50)
    public String getRegisteredname() {
        return registeredname;
    }

    public void setRegisteredname(String registeredname) {
        this.registeredname = registeredname;
    }

    @Basic
    @Column(name = "PHARMAREGNUM", nullable = true, length = 50)
    public String getPharmaregnum() {
        return pharmaregnum;
    }

    public void setPharmaregnum(String pharmaregnum) {
        this.pharmaregnum = pharmaregnum;
    }

    @Basic
    @Column(name = "PHARMAEXPIREYDATE", nullable = true, length = 50)
    public String getPharmaexpireydate() {
        return pharmaexpireydate;
    }

    public void setPharmaexpireydate(String pharmaexpireydate) {
        this.pharmaexpireydate = pharmaexpireydate;
    }

    @Basic
    @Column(name = "MEDPAGENUM", nullable = true, length = 50)
    public String getMedpagenum() {
        return medpagenum;
    }

    public void setMedpagenum(String medpagenum) {
        this.medpagenum = medpagenum;
    }

    @Basic
    @Column(name = "VATNUM", nullable = true, length = 50)
    public String getVatnum() {
        return vatnum;
    }

    public void setVatnum(String vatnum) {
        this.vatnum = vatnum;
    }

    @Basic
    @Column(name = "CREDITLIMIT", nullable = true, precision = 8)
    public BigDecimal getCreditlimit() {
        return creditlimit;
    }

    public void setCreditlimit(BigDecimal creditlimit) {
        this.creditlimit = creditlimit;
    }

    @Basic
    @Column(name = "INSUREDCREDITLIMIT", nullable = true, precision = 8)
    public BigDecimal getInsuredcreditlimit() {
        return insuredcreditlimit;
    }

    public void setInsuredcreditlimit(BigDecimal insuredcreditlimit) {
        this.insuredcreditlimit = insuredcreditlimit;
    }

    @Basic
    @Column(name = "INSUREDCREDITLIMITCURRENCY", nullable = true, length = 50)
    public String getInsuredcreditlimitcurrency() {
        return insuredcreditlimitcurrency;
    }

    public void setInsuredcreditlimitcurrency(String insuredcreditlimitcurrency) {
        this.insuredcreditlimitcurrency = insuredcreditlimitcurrency;
    }

    @Basic
    @Column(name = "MONTHENDDATE", nullable = true)
    public Date getMonthenddate() {
        return monthenddate;
    }

    public void setMonthenddate(Date monthenddate) {
        this.monthenddate = monthenddate;
    }

    @Basic
    @Column(name = "DIVISION", nullable = true, length = 50)
    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    @Basic
    @Column(name = "ROUTE", nullable = true, length = 50)
    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @Basic
    @Column(name = "INSURANCENUM", nullable = true, length = 50)
    public String getInsurancenum() {
        return insurancenum;
    }

    public void setInsurancenum(String insurancenum) {
        this.insurancenum = insurancenum;
    }

    @Basic
    @Column(name = "POLICYEXPIRYDATE", nullable = true)
    public Date getPolicyexpirydate() {
        return policyexpirydate;
    }

    public void setPolicyexpirydate(Date policyexpirydate) {
        this.policyexpirydate = policyexpirydate;
    }

    @Basic
    @Column(name = "DISTRIBUTORCODE", nullable = true, length = 50)
    public String getDistributorcode() {
        return distributorcode;
    }

    public void setDistributorcode(String distributorcode) {
        this.distributorcode = distributorcode;
    }

    @Basic
    @Column(name = "DOCDATETIME", nullable = true)
    public Timestamp getDocdatetime() {
        return docdatetime;
    }

    public void setDocdatetime(Timestamp docdatetime) {
        this.docdatetime = docdatetime;
    }

    @Basic
    @Column(name = "STATUSCODE", nullable = true, length = 50)
    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    @Basic
    @Column(name = "STATUSDESCR", nullable = true, length = 50)
    public String getStatusdescr() {
        return statusdescr;
    }

    public void setStatusdescr(String statusdescr) {
        this.statusdescr = statusdescr;
    }

    @Basic
    @Column(name = "MARKETSEGMENT", nullable = true, length = 50)
    public String getMarketsegment() {
        return marketsegment;
    }

    public void setMarketsegment(String marketsegment) {
        this.marketsegment = marketsegment;
    }

    @Basic
    @Column(name = "CUSTOMERLASTUPD", nullable = true)
    public Timestamp getCustomerlastupd() {
        return customerlastupd;
    }

    public void setCustomerlastupd(Timestamp customerlastupd) {
        this.customerlastupd = customerlastupd;
    }

    @Basic
    @Column(name = "STMTDAY", nullable = true)
    public Short getStmtday() {
        return stmtday;
    }

    public void setStmtday(Short stmtday) {
        this.stmtday = stmtday;
    }

    @Basic
    @Column(name = "VIEWSITESTATISTICALFORECAST", nullable = true)
    public Short getViewsitestatisticalforecast() {
        return viewsitestatisticalforecast;
    }

    public void setViewsitestatisticalforecast(Short viewsitestatisticalforecast) {
        this.viewsitestatisticalforecast = viewsitestatisticalforecast;
    }

    @Basic
    @Column(name = "FORECASTWINDOW", nullable = true)
    public Short getForecastwindow() {
        return forecastwindow;
    }

    public void setForecastwindow(Short forecastwindow) {
        this.forecastwindow = forecastwindow;
    }

    @Basic
    @Column(name = "FORECASTMANDATORY", nullable = true)
    public Short getForecastmandatory() {
        return forecastmandatory;
    }

    public void setForecastmandatory(Short forecastmandatory) {
        this.forecastmandatory = forecastmandatory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomersEntity that = (CustomersEntity) o;

        if (customerPk != that.customerPk) return false;
        if (activestatus != that.activestatus) return false;
        if (businessentityPk != that.businessentityPk) return false;
        if (lasttouched != null ? !lasttouched.equals(that.lasttouched) : that.lasttouched != null) return false;
        if (lasttoucheduser != null ? !lasttoucheduser.equals(that.lasttoucheduser) : that.lasttoucheduser != null)
            return false;
        if (edinum != null ? !edinum.equals(that.edinum) : that.edinum != null) return false;
        if (customercode != null ? !customercode.equals(that.customercode) : that.customercode != null) return false;
        if (parentaccnum != null ? !parentaccnum.equals(that.parentaccnum) : that.parentaccnum != null) return false;
        if (customername != null ? !customername.equals(that.customername) : that.customername != null) return false;
        if (registeredname != null ? !registeredname.equals(that.registeredname) : that.registeredname != null)
            return false;
        if (pharmaregnum != null ? !pharmaregnum.equals(that.pharmaregnum) : that.pharmaregnum != null) return false;
        if (pharmaexpireydate != null ? !pharmaexpireydate.equals(that.pharmaexpireydate) : that.pharmaexpireydate != null)
            return false;
        if (medpagenum != null ? !medpagenum.equals(that.medpagenum) : that.medpagenum != null) return false;
        if (vatnum != null ? !vatnum.equals(that.vatnum) : that.vatnum != null) return false;
        if (creditlimit != null ? !creditlimit.equals(that.creditlimit) : that.creditlimit != null) return false;
        if (insuredcreditlimit != null ? !insuredcreditlimit.equals(that.insuredcreditlimit) : that.insuredcreditlimit != null)
            return false;
        if (insuredcreditlimitcurrency != null ? !insuredcreditlimitcurrency.equals(that.insuredcreditlimitcurrency) : that.insuredcreditlimitcurrency != null)
            return false;
        if (monthenddate != null ? !monthenddate.equals(that.monthenddate) : that.monthenddate != null) return false;
        if (division != null ? !division.equals(that.division) : that.division != null) return false;
        if (route != null ? !route.equals(that.route) : that.route != null) return false;
        if (insurancenum != null ? !insurancenum.equals(that.insurancenum) : that.insurancenum != null) return false;
        if (policyexpirydate != null ? !policyexpirydate.equals(that.policyexpirydate) : that.policyexpirydate != null)
            return false;
        if (distributorcode != null ? !distributorcode.equals(that.distributorcode) : that.distributorcode != null)
            return false;
        if (docdatetime != null ? !docdatetime.equals(that.docdatetime) : that.docdatetime != null) return false;
        if (statuscode != null ? !statuscode.equals(that.statuscode) : that.statuscode != null) return false;
        if (statusdescr != null ? !statusdescr.equals(that.statusdescr) : that.statusdescr != null) return false;
        if (marketsegment != null ? !marketsegment.equals(that.marketsegment) : that.marketsegment != null)
            return false;
        if (customerlastupd != null ? !customerlastupd.equals(that.customerlastupd) : that.customerlastupd != null)
            return false;
        if (stmtday != null ? !stmtday.equals(that.stmtday) : that.stmtday != null) return false;
        if (viewsitestatisticalforecast != null ? !viewsitestatisticalforecast.equals(that.viewsitestatisticalforecast) : that.viewsitestatisticalforecast != null)
            return false;
        if (forecastwindow != null ? !forecastwindow.equals(that.forecastwindow) : that.forecastwindow != null)
            return false;
        if (forecastmandatory != null ? !forecastmandatory.equals(that.forecastmandatory) : that.forecastmandatory != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (customerPk ^ (customerPk >>> 32));
        result = 31 * result + (lasttouched != null ? lasttouched.hashCode() : 0);
        result = 31 * result + (lasttoucheduser != null ? lasttoucheduser.hashCode() : 0);
        result = 31 * result + (int) activestatus;
        result = 31 * result + (int) (businessentityPk ^ (businessentityPk >>> 32));
        result = 31 * result + (edinum != null ? edinum.hashCode() : 0);
        result = 31 * result + (customercode != null ? customercode.hashCode() : 0);
        result = 31 * result + (parentaccnum != null ? parentaccnum.hashCode() : 0);
        result = 31 * result + (customername != null ? customername.hashCode() : 0);
        result = 31 * result + (registeredname != null ? registeredname.hashCode() : 0);
        result = 31 * result + (pharmaregnum != null ? pharmaregnum.hashCode() : 0);
        result = 31 * result + (pharmaexpireydate != null ? pharmaexpireydate.hashCode() : 0);
        result = 31 * result + (medpagenum != null ? medpagenum.hashCode() : 0);
        result = 31 * result + (vatnum != null ? vatnum.hashCode() : 0);
        result = 31 * result + (creditlimit != null ? creditlimit.hashCode() : 0);
        result = 31 * result + (insuredcreditlimit != null ? insuredcreditlimit.hashCode() : 0);
        result = 31 * result + (insuredcreditlimitcurrency != null ? insuredcreditlimitcurrency.hashCode() : 0);
        result = 31 * result + (monthenddate != null ? monthenddate.hashCode() : 0);
        result = 31 * result + (division != null ? division.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + (insurancenum != null ? insurancenum.hashCode() : 0);
        result = 31 * result + (policyexpirydate != null ? policyexpirydate.hashCode() : 0);
        result = 31 * result + (distributorcode != null ? distributorcode.hashCode() : 0);
        result = 31 * result + (docdatetime != null ? docdatetime.hashCode() : 0);
        result = 31 * result + (statuscode != null ? statuscode.hashCode() : 0);
        result = 31 * result + (statusdescr != null ? statusdescr.hashCode() : 0);
        result = 31 * result + (marketsegment != null ? marketsegment.hashCode() : 0);
        result = 31 * result + (customerlastupd != null ? customerlastupd.hashCode() : 0);
        result = 31 * result + (stmtday != null ? stmtday.hashCode() : 0);
        result = 31 * result + (viewsitestatisticalforecast != null ? viewsitestatisticalforecast.hashCode() : 0);
        result = 31 * result + (forecastwindow != null ? forecastwindow.hashCode() : 0);
        result = 31 * result + (forecastmandatory != null ? forecastmandatory.hashCode() : 0);
        return result;
    }
}
