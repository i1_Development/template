
package solutions.i1.labs.integration;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the solutions.i1.labs.integration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DocumentCount_QNAME = new QName("", "DocumentCount");
    private final static QName _CountryID_QNAME = new QName("", "CountryID");
    private final static QName _PurchaseQtyPerPackUnit_QNAME = new QName("", "PurchaseQtyPerPackUnit");
    private final static QName _FirmPeriod_QNAME = new QName("", "FirmPeriod");
    private final static QName _PresentationDescription_QNAME = new QName("", "PresentationDescription");
    private final static QName _SalesUnitVolume_QNAME = new QName("", "SalesUnitVolume");
    private final static QName _Size_QNAME = new QName("", "Size");
    private final static QName _InsuranceNumber_QNAME = new QName("", "InsuranceNumber");
    private final static QName _ItemType_QNAME = new QName("", "ItemType");
    private final static QName _AccountCategory_QNAME = new QName("", "AccountCategory");
    private final static QName _SalesVatGroup_QNAME = new QName("", "SalesVatGroup");
    private final static QName _MarketSegment_QNAME = new QName("", "MarketSegment");
    private final static QName _IncoTerms_QNAME = new QName("", "IncoTerms");
    private final static QName _RegisteredName_QNAME = new QName("", "RegisteredName");
    private final static QName _ForecastMandatory_QNAME = new QName("", "ForecastMandatory");
    private final static QName _SalesWeightUnit_QNAME = new QName("", "SalesWeightUnit");
    private final static QName _FaxNumber_QNAME = new QName("", "FaxNumber");
    private final static QName _ID_QNAME = new QName("", "ID");
    private final static QName _MaximumInventory_QNAME = new QName("", "MaximumInventory");
    private final static QName _NarcoticIngredientType_QNAME = new QName("", "NarcoticIngredientType");
    private final static QName _ComponentID_QNAME = new QName("", "ComponentID");
    private final static QName _PharmacyExpiryDate_QNAME = new QName("", "PharmacyExpiryDate");
    private final static QName _PlanningTimeFence_QNAME = new QName("", "PlanningTimeFence");
    private final static QName _Lifecycle_QNAME = new QName("", "Lifecycle");
    private final static QName _SafetyStockLevel_QNAME = new QName("", "SafetyStockLevel");
    private final static QName _ProductType_QNAME = new QName("", "ProductType");
    private final static QName _PurchaseUnitHeight_QNAME = new QName("", "PurchaseUnitHeight");
    private final static QName _SignalCode_QNAME = new QName("", "SignalCode");
    private final static QName _TaxID_QNAME = new QName("", "TaxID");
    private final static QName _Code_QNAME = new QName("", "Code");
    private final static QName _EmailAddress_QNAME = new QName("", "EmailAddress");
    private final static QName _SupplierPurchaseCode_QNAME = new QName("", "SupplierPurchaseCode");
    private final static QName _NappiCode_QNAME = new QName("", "NappiCode");
    private final static QName _CitySubDivisionName_QNAME = new QName("", "CitySubDivisionName");
    private final static QName _PurchaseUnitLength_QNAME = new QName("", "PurchaseUnitLength");
    private final static QName _ItemGroupID_QNAME = new QName("", "ItemGroupID");
    private final static QName _BODID_QNAME = new QName("", "BODID");
    private final static QName _ModeOfTransport_QNAME = new QName("", "ModeOfTransport");
    private final static QName _ToleranceDays_QNAME = new QName("", "ToleranceDays");
    private final static QName _InventoryItemCode_QNAME = new QName("", "InventoryItemCode");
    private final static QName _PurchaseItemsPerUnit_QNAME = new QName("", "PurchaseItemsPerUnit");
    private final static QName _AccountArea_QNAME = new QName("", "AccountArea");
    private final static QName _PurchaseHeightUnit_QNAME = new QName("", "PurchaseHeightUnit");
    private final static QName _SalesFocusGroup_QNAME = new QName("", "SalesFocusGroup");
    private final static QName _DialNumber_QNAME = new QName("", "DialNumber");
    private final static QName _OrderInterval_QNAME = new QName("", "OrderInterval");
    private final static QName _DistributorDescription_QNAME = new QName("", "DistributorDescription");
    private final static QName _CountryDialing_QNAME = new QName("", "CountryDialing");
    private final static QName _Description_QNAME = new QName("", "Description");
    private final static QName _BusinessUnitDescription_QNAME = new QName("", "BusinessUnitDescription");
    private final static QName _MarketTypeDescription_QNAME = new QName("", "MarketTypeDescription");
    private final static QName _SalesVolumeUnit_QNAME = new QName("", "SalesVolumeUnit");
    private final static QName _StatementDay_QNAME = new QName("", "StatementDay");
    private final static QName _DistributionChannel_QNAME = new QName("", "DistributionChannel");
    private final static QName _ForeignName_QNAME = new QName("", "ForeignName");
    private final static QName _ScheduleCode_QNAME = new QName("", "ScheduleCode");
    private final static QName _JobTitle_QNAME = new QName("", "JobTitle");
    private final static QName _PurchasePackagingUnit_QNAME = new QName("", "PurchasePackagingUnit");
    private final static QName _StorageUnitOfMeasure_QNAME = new QName("", "StorageUnitOfMeasure");
    private final static QName _CustomerLastUpdateDate_QNAME = new QName("", "CustomerLastUpdateDate");
    private final static QName _SalesPackagingUnit_QNAME = new QName("", "SalesPackagingUnit");
    private final static QName _AccountCurrency_QNAME = new QName("", "AccountCurrency");
    private final static QName _CommodityCode_QNAME = new QName("", "CommodityCode");
    private final static QName _PurchaseLengthUnit_QNAME = new QName("", "PurchaseLengthUnit");
    private final static QName _Division_QNAME = new QName("", "Division");
    private final static QName _EANCode_QNAME = new QName("", "EANCode");
    private final static QName _DocumentDateTime_QNAME = new QName("", "DocumentDateTime");
    private final static QName _PurchaseUnitWeight_QNAME = new QName("", "PurchaseUnitWeight");
    private final static QName _SiteID_QNAME = new QName("", "SiteID");
    private final static QName _TaskID_QNAME = new QName("", "TaskID");
    private final static QName _SupplierItemID_QNAME = new QName("", "SupplierItemID");
    private final static QName _CustomerCode_QNAME = new QName("", "CustomerCode");
    private final static QName _MedPageNumber_QNAME = new QName("", "MedPageNumber");
    private final static QName _EndMarketCountry_QNAME = new QName("", "EndMarketCountry");
    private final static QName _MaximumOrderQuantity_QNAME = new QName("", "MaximumOrderQuantity");
    private final static QName _DistributionFeeItemGroup_QNAME = new QName("", "DistributionFeeItemGroup");
    private final static QName _SignalCodeDescription_QNAME = new QName("", "SignalCodeDescription");
    private final static QName _ItemGroup_QNAME = new QName("", "ItemGroup");
    private final static QName _CommonPackID_QNAME = new QName("", "CommonPackID");
    private final static QName _LineOfBusiness_QNAME = new QName("", "LineOfBusiness");
    private final static QName _CellNumber_QNAME = new QName("", "CellNumber");
    private final static QName _ParentID_QNAME = new QName("", "ParentID");
    private final static QName _OverseasItem_QNAME = new QName("", "OverseasItem");
    private final static QName _WarehouseDescription_QNAME = new QName("", "WarehouseDescription");
    private final static QName _DistributorCode_QNAME = new QName("", "DistributorCode");
    private final static QName _PolicyExpiryDate_QNAME = new QName("", "PolicyExpiryDate");
    private final static QName _BusinessUnitID_QNAME = new QName("", "BusinessUnitID");
    private final static QName _PharmacyRegistrationNumber_QNAME = new QName("", "PharmacyRegistrationNumber");
    private final static QName _DefaultSupplier_QNAME = new QName("", "DefaultSupplier");
    private final static QName _WarehouseID_QNAME = new QName("", "WarehouseID");
    private final static QName _PharmaceuticalSchedule_QNAME = new QName("", "PharmaceuticalSchedule");
    private final static QName _SalesUnitHeight_QNAME = new QName("", "SalesUnitHeight");
    private final static QName _Responsibility_QNAME = new QName("", "Responsibility");
    private final static QName _Dosage_QNAME = new QName("", "Dosage");
    private final static QName _Name_QNAME = new QName("", "Name");
    private final static QName _CountryDescription_QNAME = new QName("", "CountryDescription");
    private final static QName _MarketTypeID_QNAME = new QName("", "MarketTypeID");
    private final static QName _BusinessPartnerType_QNAME = new QName("", "BusinessPartnerType");
    private final static QName _SellItemCustomerCode_QNAME = new QName("", "SellItemCustomerCode");
    private final static QName _PurchaseWidthUnit_QNAME = new QName("", "PurchaseWidthUnit");
    private final static QName _ItemGroupDescription_QNAME = new QName("", "ItemGroupDescription");
    private final static QName _ConversionUnit_QNAME = new QName("", "ConversionUnit");
    private final static QName _TherapeuticAreaDescription_QNAME = new QName("", "TherapeuticAreaDescription");
    private final static QName _RegionID_QNAME = new QName("", "RegionID");
    private final static QName _SalesUnitWidth_QNAME = new QName("", "SalesUnitWidth");
    private final static QName _Route_QNAME = new QName("", "Route");
    private final static QName _LotControl_QNAME = new QName("", "LotControl");
    private final static QName _PeriodSLED_QNAME = new QName("", "PeriodSLED");
    private final static QName _MonthEndDate_QNAME = new QName("", "MonthEndDate");
    private final static QName _PlanningSystem_QNAME = new QName("", "PlanningSystem");
    private final static QName _UnitOfMeasure_QNAME = new QName("", "UnitOfMeasure");
    private final static QName _PurchaseUnit_QNAME = new QName("", "PurchaseUnit");
    private final static QName _LogicalID_QNAME = new QName("", "LogicalID");
    private final static QName _OrderLeadTime_QNAME = new QName("", "OrderLeadTime");
    private final static QName _HarmonisedTariffCode_QNAME = new QName("", "HarmonisedTariffCode");
    private final static QName _SalesUnitLength_QNAME = new QName("", "SalesUnitLength");
    private final static QName _MaterialHolder_QNAME = new QName("", "MaterialHolder");
    private final static QName _ManufacturerItemID_QNAME = new QName("", "ManufacturerItemID");
    private final static QName _ABCCode_QNAME = new QName("", "ABCCode");
    private final static QName _CityName_QNAME = new QName("", "CityName");
    private final static QName _BrandID_QNAME = new QName("", "BrandID");
    private final static QName _ItemID_QNAME = new QName("", "ItemID");
    private final static QName _RegionDescription_QNAME = new QName("", "RegionDescription");
    private final static QName _CountrySubDivisionCode_QNAME = new QName("", "CountrySubDivisionCode");
    private final static QName _IncrementalOrderQuantity_QNAME = new QName("", "IncrementalOrderQuantity");
    private final static QName _CustomerLastUpdateTime_QNAME = new QName("", "CustomerLastUpdateTime");
    private final static QName _DepartmentName_QNAME = new QName("", "DepartmentName");
    private final static QName _PaymentMethod_QNAME = new QName("", "PaymentMethod");
    private final static QName _SalesHeightUnit_QNAME = new QName("", "SalesHeightUnit");
    private final static QName _ProductManagerID_QNAME = new QName("", "ProductManagerID");
    private final static QName _PurchaseVATGroup_QNAME = new QName("", "PurchaseVATGroup");
    private final static QName _ShipType_QNAME = new QName("", "ShipType");
    private final static QName _BrandDescription_QNAME = new QName("", "BrandDescription");
    private final static QName _SalesQtyPerPackUnit_QNAME = new QName("", "SalesQtyPerPackUnit");
    private final static QName _VATNumber_QNAME = new QName("", "VATNumber");
    private final static QName _DistributorID_QNAME = new QName("", "DistributorID");
    private final static QName _FirmCode_QNAME = new QName("", "FirmCode");
    private final static QName _ItemLivery_QNAME = new QName("", "ItemLivery");
    private final static QName _SalesWidthUnit_QNAME = new QName("", "SalesWidthUnit");
    private final static QName _ProductManager_QNAME = new QName("", "ProductManager");
    private final static QName _PostalCode_QNAME = new QName("", "PostalCode");
    private final static QName _ChannelCode_QNAME = new QName("", "ChannelCode");
    private final static QName _PaymentTerms_QNAME = new QName("", "PaymentTerms");
    private final static QName _ProductManagerDescription_QNAME = new QName("", "ProductManagerDescription");
    private final static QName _CommonPackDescription_QNAME = new QName("", "CommonPackDescription");
    private final static QName _GLMethod_QNAME = new QName("", "GLMethod");
    private final static QName _SafetyTime_QNAME = new QName("", "SafetyTime");
    private final static QName _PurchaseUnitWidth_QNAME = new QName("", "PurchaseUnitWidth");
    private final static QName _ValuationMethod_QNAME = new QName("", "ValuationMethod");
    private final static QName _CustomerItemID_QNAME = new QName("", "CustomerItemID");
    private final static QName _EDINumber_QNAME = new QName("", "EDINumber");
    private final static QName _ProductClassification_QNAME = new QName("", "ProductClassification");
    private final static QName _TariffCode_QNAME = new QName("", "TariffCode");
    private final static QName _PurchaseWeightUnit_QNAME = new QName("", "PurchaseWeightUnit");
    private final static QName _CommonItem_QNAME = new QName("", "CommonItem");
    private final static QName _PresentationID_QNAME = new QName("", "PresentationID");
    private final static QName _CustomerName_QNAME = new QName("", "CustomerName");
    private final static QName _CountryCode_QNAME = new QName("", "CountryCode");
    private final static QName _SalesUnitWeight_QNAME = new QName("", "SalesUnitWeight");
    private final static QName _SalesLengthUnit_QNAME = new QName("", "SalesLengthUnit");
    private final static QName _MinimumRemainingShelflife_QNAME = new QName("", "MinimumRemainingShelflife");
    private final static QName _SignalCodeID_QNAME = new QName("", "SignalCodeID");
    private final static QName _VATIndicator_QNAME = new QName("", "VATIndicator");
    private final static QName _SalesItemsPerUnit_QNAME = new QName("", "SalesItemsPerUnit");
    private final static QName _ForecastWindow_QNAME = new QName("", "ForecastWindow");
    private final static QName _Zone_QNAME = new QName("", "Zone");
    private final static QName _PurchaseUnitVolume_QNAME = new QName("", "PurchaseUnitVolume");
    private final static QName _BlockingDays_QNAME = new QName("", "BlockingDays");
    private final static QName _Material_QNAME = new QName("", "Material");
    private final static QName _AccountNumber_QNAME = new QName("", "AccountNumber");
    private final static QName _ControlCode_QNAME = new QName("", "ControlCode");
    private final static QName _AreaDialing_QNAME = new QName("", "AreaDialing");
    private final static QName _Extension_QNAME = new QName("", "Extension");
    private final static QName _TherapeuticAreaID_QNAME = new QName("", "TherapeuticAreaID");
    private final static QName _CreationDateTime_QNAME = new QName("", "CreationDateTime");
    private final static QName _ReferenceID_QNAME = new QName("", "ReferenceID");
    private final static QName _SelectionCode_QNAME = new QName("", "SelectionCode");
    private final static QName _ShipmentDate_QNAME = new QName("", "ShipmentDate");
    private final static QName _ItemRelationshipCode_QNAME = new QName("", "ItemRelationshipCode");
    private final static QName _MinimumOrderQuantity_QNAME = new QName("", "MinimumOrderQuantity");
    private final static QName _ParentAccountNumber_QNAME = new QName("", "ParentAccountNumber");
    private final static QName _ViewSiteStatisticalForecast_QNAME = new QName("", "ViewSiteStatisticalForecast");
    private final static QName _ItemDivision_QNAME = new QName("", "ItemDivision");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: solutions.i1.labs.integration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NetNetWeightDimension }
     * 
     */
    public NetNetWeightDimension createNetNetWeightDimension() {
        return new NetNetWeightDimension();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link Party }
     * 
     */
    public Party createParty() {
        return new Party();
    }

    /**
     * Create an instance of {@link PartyIDs }
     * 
     */
    public PartyIDs createPartyIDs() {
        return new PartyIDs();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link AddressLine }
     * 
     */
    public AddressLine createAddressLine() {
        return new AddressLine();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Communication }
     * 
     */
    public Communication createCommunication() {
        return new Communication();
    }

    /**
     * Create an instance of {@link DefaultWarehouse }
     * 
     */
    public DefaultWarehouse createDefaultWarehouse() {
        return new DefaultWarehouse();
    }

    /**
     * Create an instance of {@link InventoryGroup }
     * 
     */
    public InventoryGroup createInventoryGroup() {
        return new InventoryGroup();
    }

    /**
     * Create an instance of {@link MaterialInventoryGroup }
     * 
     */
    public MaterialInventoryGroup createMaterialInventoryGroup() {
        return new MaterialInventoryGroup();
    }

    /**
     * Create an instance of {@link CreditLimit }
     * 
     */
    public CreditLimit createCreditLimit() {
        return new CreditLimit();
    }

    /**
     * Create an instance of {@link Length }
     * 
     */
    public Length createLength() {
        return new Length();
    }

    /**
     * Create an instance of {@link MaterialCustomer }
     * 
     */
    public MaterialCustomer createMaterialCustomer() {
        return new MaterialCustomer();
    }

    /**
     * Create an instance of {@link InsuredCreditLimit }
     * 
     */
    public InsuredCreditLimit createInsuredCreditLimit() {
        return new InsuredCreditLimit();
    }

    /**
     * Create an instance of {@link Insurance }
     * 
     */
    public Insurance createInsurance() {
        return new Insurance();
    }

    /**
     * Create an instance of {@link TIs }
     * 
     */
    public TIs createTIs() {
        return new TIs();
    }

    /**
     * Create an instance of {@link NetWeightDimension }
     * 
     */
    public NetWeightDimension createNetWeightDimension() {
        return new NetWeightDimension();
    }

    /**
     * Create an instance of {@link Weight }
     * 
     */
    public Weight createWeight() {
        return new Weight();
    }

    /**
     * Create an instance of {@link ShipmentDates }
     * 
     */
    public ShipmentDates createShipmentDates() {
        return new ShipmentDates();
    }

    /**
     * Create an instance of {@link PhysicalDimension }
     * 
     */
    public PhysicalDimension createPhysicalDimension() {
        return new PhysicalDimension();
    }

    /**
     * Create an instance of {@link GrossWeightDimension }
     * 
     */
    public GrossWeightDimension createGrossWeightDimension() {
        return new GrossWeightDimension();
    }

    /**
     * Create an instance of {@link CubeDimension }
     * 
     */
    public CubeDimension createCubeDimension() {
        return new CubeDimension();
    }

    /**
     * Create an instance of {@link Warehouse }
     * 
     */
    public Warehouse createWarehouse() {
        return new Warehouse();
    }

    /**
     * Create an instance of {@link DataArea }
     * 
     */
    public DataArea createDataArea() {
        return new DataArea();
    }

    /**
     * Create an instance of {@link CustomerItemMaster }
     * 
     */
    public CustomerItemMaster createCustomerItemMaster() {
        return new CustomerItemMaster();
    }

    /**
     * Create an instance of {@link ApplicationArea }
     * 
     */
    public ApplicationArea createApplicationArea() {
        return new ApplicationArea();
    }

    /**
     * Create an instance of {@link Sender }
     * 
     */
    public Sender createSender() {
        return new Sender();
    }

    /**
     * Create an instance of {@link Receiver }
     * 
     */
    public Receiver createReceiver() {
        return new Receiver();
    }

    /**
     * Create an instance of {@link Customers }
     * 
     */
    public Customers createCustomers() {
        return new Customers();
    }

    /**
     * Create an instance of {@link EAN }
     * 
     */
    public EAN createEAN() {
        return new EAN();
    }

    /**
     * Create an instance of {@link ConversionFactor }
     * 
     */
    public ConversionFactor createConversionFactor() {
        return new ConversionFactor();
    }

    /**
     * Create an instance of {@link Width }
     * 
     */
    public Width createWidth() {
        return new Width();
    }

    /**
     * Create an instance of {@link Height }
     * 
     */
    public Height createHeight() {
        return new Height();
    }

    /**
     * Create an instance of {@link HIs }
     * 
     */
    public HIs createHIs() {
        return new HIs();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DocumentCount")
    public JAXBElement<String> createDocumentCount(String value) {
        return new JAXBElement<String>(_DocumentCount_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountryID")
    public JAXBElement<String> createCountryID(String value) {
        return new JAXBElement<String>(_CountryID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseQtyPerPackUnit")
    public JAXBElement<String> createPurchaseQtyPerPackUnit(String value) {
        return new JAXBElement<String>(_PurchaseQtyPerPackUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirmPeriod")
    public JAXBElement<String> createFirmPeriod(String value) {
        return new JAXBElement<String>(_FirmPeriod_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PresentationDescription")
    public JAXBElement<String> createPresentationDescription(String value) {
        return new JAXBElement<String>(_PresentationDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesUnitVolume")
    public JAXBElement<String> createSalesUnitVolume(String value) {
        return new JAXBElement<String>(_SalesUnitVolume_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Size")
    public JAXBElement<String> createSize(String value) {
        return new JAXBElement<String>(_Size_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InsuranceNumber")
    public JAXBElement<String> createInsuranceNumber(String value) {
        return new JAXBElement<String>(_InsuranceNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemType")
    public JAXBElement<String> createItemType(String value) {
        return new JAXBElement<String>(_ItemType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccountCategory")
    public JAXBElement<String> createAccountCategory(String value) {
        return new JAXBElement<String>(_AccountCategory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesVatGroup")
    public JAXBElement<String> createSalesVatGroup(String value) {
        return new JAXBElement<String>(_SalesVatGroup_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MarketSegment")
    public JAXBElement<String> createMarketSegment(String value) {
        return new JAXBElement<String>(_MarketSegment_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncoTerms")
    public JAXBElement<String> createIncoTerms(String value) {
        return new JAXBElement<String>(_IncoTerms_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RegisteredName")
    public JAXBElement<String> createRegisteredName(String value) {
        return new JAXBElement<String>(_RegisteredName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ForecastMandatory")
    public JAXBElement<String> createForecastMandatory(String value) {
        return new JAXBElement<String>(_ForecastMandatory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesWeightUnit")
    public JAXBElement<String> createSalesWeightUnit(String value) {
        return new JAXBElement<String>(_SalesWeightUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FaxNumber")
    public JAXBElement<String> createFaxNumber(String value) {
        return new JAXBElement<String>(_FaxNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ID")
    public JAXBElement<String> createID(String value) {
        return new JAXBElement<String>(_ID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MaximumInventory")
    public JAXBElement<String> createMaximumInventory(String value) {
        return new JAXBElement<String>(_MaximumInventory_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NarcoticIngredientType")
    public JAXBElement<String> createNarcoticIngredientType(String value) {
        return new JAXBElement<String>(_NarcoticIngredientType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ComponentID")
    public JAXBElement<String> createComponentID(String value) {
        return new JAXBElement<String>(_ComponentID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PharmacyExpiryDate")
    public JAXBElement<String> createPharmacyExpiryDate(String value) {
        return new JAXBElement<String>(_PharmacyExpiryDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanningTimeFence")
    public JAXBElement<String> createPlanningTimeFence(String value) {
        return new JAXBElement<String>(_PlanningTimeFence_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Lifecycle")
    public JAXBElement<String> createLifecycle(String value) {
        return new JAXBElement<String>(_Lifecycle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SafetyStockLevel")
    public JAXBElement<String> createSafetyStockLevel(String value) {
        return new JAXBElement<String>(_SafetyStockLevel_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductType")
    public JAXBElement<String> createProductType(String value) {
        return new JAXBElement<String>(_ProductType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnitHeight")
    public JAXBElement<String> createPurchaseUnitHeight(String value) {
        return new JAXBElement<String>(_PurchaseUnitHeight_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SignalCode")
    public JAXBElement<String> createSignalCode(String value) {
        return new JAXBElement<String>(_SignalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TaxID")
    public JAXBElement<String> createTaxID(String value) {
        return new JAXBElement<String>(_TaxID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Code")
    public JAXBElement<String> createCode(String value) {
        return new JAXBElement<String>(_Code_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmailAddress")
    public JAXBElement<String> createEmailAddress(String value) {
        return new JAXBElement<String>(_EmailAddress_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SupplierPurchaseCode")
    public JAXBElement<String> createSupplierPurchaseCode(String value) {
        return new JAXBElement<String>(_SupplierPurchaseCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NappiCode")
    public JAXBElement<String> createNappiCode(String value) {
        return new JAXBElement<String>(_NappiCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CitySubDivisionName")
    public JAXBElement<String> createCitySubDivisionName(String value) {
        return new JAXBElement<String>(_CitySubDivisionName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnitLength")
    public JAXBElement<String> createPurchaseUnitLength(String value) {
        return new JAXBElement<String>(_PurchaseUnitLength_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemGroupID")
    public JAXBElement<String> createItemGroupID(String value) {
        return new JAXBElement<String>(_ItemGroupID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BODID")
    public JAXBElement<String> createBODID(String value) {
        return new JAXBElement<String>(_BODID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ModeOfTransport")
    public JAXBElement<String> createModeOfTransport(String value) {
        return new JAXBElement<String>(_ModeOfTransport_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ToleranceDays")
    public JAXBElement<String> createToleranceDays(String value) {
        return new JAXBElement<String>(_ToleranceDays_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InventoryItemCode")
    public JAXBElement<String> createInventoryItemCode(String value) {
        return new JAXBElement<String>(_InventoryItemCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseItemsPerUnit")
    public JAXBElement<String> createPurchaseItemsPerUnit(String value) {
        return new JAXBElement<String>(_PurchaseItemsPerUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccountArea")
    public JAXBElement<String> createAccountArea(String value) {
        return new JAXBElement<String>(_AccountArea_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseHeightUnit")
    public JAXBElement<String> createPurchaseHeightUnit(String value) {
        return new JAXBElement<String>(_PurchaseHeightUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesFocusGroup")
    public JAXBElement<String> createSalesFocusGroup(String value) {
        return new JAXBElement<String>(_SalesFocusGroup_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DialNumber")
    public JAXBElement<String> createDialNumber(String value) {
        return new JAXBElement<String>(_DialNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OrderInterval")
    public JAXBElement<String> createOrderInterval(String value) {
        return new JAXBElement<String>(_OrderInterval_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DistributorDescription")
    public JAXBElement<String> createDistributorDescription(String value) {
        return new JAXBElement<String>(_DistributorDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountryDialing")
    public JAXBElement<String> createCountryDialing(String value) {
        return new JAXBElement<String>(_CountryDialing_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Description")
    public JAXBElement<String> createDescription(String value) {
        return new JAXBElement<String>(_Description_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BusinessUnitDescription")
    public JAXBElement<String> createBusinessUnitDescription(String value) {
        return new JAXBElement<String>(_BusinessUnitDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MarketTypeDescription")
    public JAXBElement<String> createMarketTypeDescription(String value) {
        return new JAXBElement<String>(_MarketTypeDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesVolumeUnit")
    public JAXBElement<String> createSalesVolumeUnit(String value) {
        return new JAXBElement<String>(_SalesVolumeUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StatementDay")
    public JAXBElement<String> createStatementDay(String value) {
        return new JAXBElement<String>(_StatementDay_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DistributionChannel")
    public JAXBElement<String> createDistributionChannel(String value) {
        return new JAXBElement<String>(_DistributionChannel_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ForeignName")
    public JAXBElement<String> createForeignName(String value) {
        return new JAXBElement<String>(_ForeignName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ScheduleCode")
    public JAXBElement<String> createScheduleCode(String value) {
        return new JAXBElement<String>(_ScheduleCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "JobTitle")
    public JAXBElement<String> createJobTitle(String value) {
        return new JAXBElement<String>(_JobTitle_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchasePackagingUnit")
    public JAXBElement<String> createPurchasePackagingUnit(String value) {
        return new JAXBElement<String>(_PurchasePackagingUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StorageUnitOfMeasure")
    public JAXBElement<String> createStorageUnitOfMeasure(String value) {
        return new JAXBElement<String>(_StorageUnitOfMeasure_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomerLastUpdateDate")
    public JAXBElement<String> createCustomerLastUpdateDate(String value) {
        return new JAXBElement<String>(_CustomerLastUpdateDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesPackagingUnit")
    public JAXBElement<String> createSalesPackagingUnit(String value) {
        return new JAXBElement<String>(_SalesPackagingUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccountCurrency")
    public JAXBElement<String> createAccountCurrency(String value) {
        return new JAXBElement<String>(_AccountCurrency_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CommodityCode")
    public JAXBElement<String> createCommodityCode(String value) {
        return new JAXBElement<String>(_CommodityCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseLengthUnit")
    public JAXBElement<String> createPurchaseLengthUnit(String value) {
        return new JAXBElement<String>(_PurchaseLengthUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Division")
    public JAXBElement<String> createDivision(String value) {
        return new JAXBElement<String>(_Division_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EANCode")
    public JAXBElement<String> createEANCode(String value) {
        return new JAXBElement<String>(_EANCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DocumentDateTime")
    public JAXBElement<String> createDocumentDateTime(String value) {
        return new JAXBElement<String>(_DocumentDateTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnitWeight")
    public JAXBElement<String> createPurchaseUnitWeight(String value) {
        return new JAXBElement<String>(_PurchaseUnitWeight_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SiteID")
    public JAXBElement<String> createSiteID(String value) {
        return new JAXBElement<String>(_SiteID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TaskID")
    public JAXBElement<String> createTaskID(String value) {
        return new JAXBElement<String>(_TaskID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SupplierItemID")
    public JAXBElement<String> createSupplierItemID(String value) {
        return new JAXBElement<String>(_SupplierItemID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomerCode")
    public JAXBElement<String> createCustomerCode(String value) {
        return new JAXBElement<String>(_CustomerCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MedPageNumber")
    public JAXBElement<String> createMedPageNumber(String value) {
        return new JAXBElement<String>(_MedPageNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EndMarketCountry")
    public JAXBElement<String> createEndMarketCountry(String value) {
        return new JAXBElement<String>(_EndMarketCountry_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MaximumOrderQuantity")
    public JAXBElement<String> createMaximumOrderQuantity(String value) {
        return new JAXBElement<String>(_MaximumOrderQuantity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DistributionFeeItemGroup")
    public JAXBElement<String> createDistributionFeeItemGroup(String value) {
        return new JAXBElement<String>(_DistributionFeeItemGroup_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SignalCodeDescription")
    public JAXBElement<String> createSignalCodeDescription(String value) {
        return new JAXBElement<String>(_SignalCodeDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemGroup")
    public JAXBElement<String> createItemGroup(String value) {
        return new JAXBElement<String>(_ItemGroup_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CommonPackID")
    public JAXBElement<String> createCommonPackID(String value) {
        return new JAXBElement<String>(_CommonPackID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LineOfBusiness")
    public JAXBElement<String> createLineOfBusiness(String value) {
        return new JAXBElement<String>(_LineOfBusiness_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CellNumber")
    public JAXBElement<String> createCellNumber(String value) {
        return new JAXBElement<String>(_CellNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ParentID")
    public JAXBElement<String> createParentID(String value) {
        return new JAXBElement<String>(_ParentID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OverseasItem")
    public JAXBElement<String> createOverseasItem(String value) {
        return new JAXBElement<String>(_OverseasItem_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WarehouseDescription")
    public JAXBElement<String> createWarehouseDescription(String value) {
        return new JAXBElement<String>(_WarehouseDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DistributorCode")
    public JAXBElement<String> createDistributorCode(String value) {
        return new JAXBElement<String>(_DistributorCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PolicyExpiryDate")
    public JAXBElement<String> createPolicyExpiryDate(String value) {
        return new JAXBElement<String>(_PolicyExpiryDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BusinessUnitID")
    public JAXBElement<String> createBusinessUnitID(String value) {
        return new JAXBElement<String>(_BusinessUnitID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PharmacyRegistrationNumber")
    public JAXBElement<String> createPharmacyRegistrationNumber(String value) {
        return new JAXBElement<String>(_PharmacyRegistrationNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DefaultSupplier")
    public JAXBElement<String> createDefaultSupplier(String value) {
        return new JAXBElement<String>(_DefaultSupplier_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WarehouseID")
    public JAXBElement<String> createWarehouseID(String value) {
        return new JAXBElement<String>(_WarehouseID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PharmaceuticalSchedule")
    public JAXBElement<String> createPharmaceuticalSchedule(String value) {
        return new JAXBElement<String>(_PharmaceuticalSchedule_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesUnitHeight")
    public JAXBElement<String> createSalesUnitHeight(String value) {
        return new JAXBElement<String>(_SalesUnitHeight_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Responsibility")
    public JAXBElement<String> createResponsibility(String value) {
        return new JAXBElement<String>(_Responsibility_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Dosage")
    public JAXBElement<String> createDosage(String value) {
        return new JAXBElement<String>(_Dosage_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountryDescription")
    public JAXBElement<String> createCountryDescription(String value) {
        return new JAXBElement<String>(_CountryDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MarketTypeID")
    public JAXBElement<String> createMarketTypeID(String value) {
        return new JAXBElement<String>(_MarketTypeID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BusinessPartnerType")
    public JAXBElement<String> createBusinessPartnerType(String value) {
        return new JAXBElement<String>(_BusinessPartnerType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SellItemCustomerCode")
    public JAXBElement<String> createSellItemCustomerCode(String value) {
        return new JAXBElement<String>(_SellItemCustomerCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseWidthUnit")
    public JAXBElement<String> createPurchaseWidthUnit(String value) {
        return new JAXBElement<String>(_PurchaseWidthUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemGroupDescription")
    public JAXBElement<String> createItemGroupDescription(String value) {
        return new JAXBElement<String>(_ItemGroupDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ConversionUnit")
    public JAXBElement<String> createConversionUnit(String value) {
        return new JAXBElement<String>(_ConversionUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TherapeuticAreaDescription")
    public JAXBElement<String> createTherapeuticAreaDescription(String value) {
        return new JAXBElement<String>(_TherapeuticAreaDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RegionID")
    public JAXBElement<String> createRegionID(String value) {
        return new JAXBElement<String>(_RegionID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesUnitWidth")
    public JAXBElement<String> createSalesUnitWidth(String value) {
        return new JAXBElement<String>(_SalesUnitWidth_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Route")
    public JAXBElement<String> createRoute(String value) {
        return new JAXBElement<String>(_Route_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LotControl")
    public JAXBElement<String> createLotControl(String value) {
        return new JAXBElement<String>(_LotControl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PeriodSLED")
    public JAXBElement<String> createPeriodSLED(String value) {
        return new JAXBElement<String>(_PeriodSLED_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MonthEndDate")
    public JAXBElement<String> createMonthEndDate(String value) {
        return new JAXBElement<String>(_MonthEndDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanningSystem")
    public JAXBElement<String> createPlanningSystem(String value) {
        return new JAXBElement<String>(_PlanningSystem_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UnitOfMeasure")
    public JAXBElement<String> createUnitOfMeasure(String value) {
        return new JAXBElement<String>(_UnitOfMeasure_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnit")
    public JAXBElement<String> createPurchaseUnit(String value) {
        return new JAXBElement<String>(_PurchaseUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LogicalID")
    public JAXBElement<String> createLogicalID(String value) {
        return new JAXBElement<String>(_LogicalID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OrderLeadTime")
    public JAXBElement<String> createOrderLeadTime(String value) {
        return new JAXBElement<String>(_OrderLeadTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HarmonisedTariffCode")
    public JAXBElement<String> createHarmonisedTariffCode(String value) {
        return new JAXBElement<String>(_HarmonisedTariffCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesUnitLength")
    public JAXBElement<String> createSalesUnitLength(String value) {
        return new JAXBElement<String>(_SalesUnitLength_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MaterialHolder")
    public JAXBElement<String> createMaterialHolder(String value) {
        return new JAXBElement<String>(_MaterialHolder_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ManufacturerItemID")
    public JAXBElement<String> createManufacturerItemID(String value) {
        return new JAXBElement<String>(_ManufacturerItemID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ABCCode")
    public JAXBElement<String> createABCCode(String value) {
        return new JAXBElement<String>(_ABCCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CityName")
    public JAXBElement<String> createCityName(String value) {
        return new JAXBElement<String>(_CityName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BrandID")
    public JAXBElement<String> createBrandID(String value) {
        return new JAXBElement<String>(_BrandID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemID")
    public JAXBElement<String> createItemID(String value) {
        return new JAXBElement<String>(_ItemID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RegionDescription")
    public JAXBElement<String> createRegionDescription(String value) {
        return new JAXBElement<String>(_RegionDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountrySubDivisionCode")
    public JAXBElement<String> createCountrySubDivisionCode(String value) {
        return new JAXBElement<String>(_CountrySubDivisionCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncrementalOrderQuantity")
    public JAXBElement<String> createIncrementalOrderQuantity(String value) {
        return new JAXBElement<String>(_IncrementalOrderQuantity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomerLastUpdateTime")
    public JAXBElement<String> createCustomerLastUpdateTime(String value) {
        return new JAXBElement<String>(_CustomerLastUpdateTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DepartmentName")
    public JAXBElement<String> createDepartmentName(String value) {
        return new JAXBElement<String>(_DepartmentName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PaymentMethod")
    public JAXBElement<String> createPaymentMethod(String value) {
        return new JAXBElement<String>(_PaymentMethod_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesHeightUnit")
    public JAXBElement<String> createSalesHeightUnit(String value) {
        return new JAXBElement<String>(_SalesHeightUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductManagerID")
    public JAXBElement<String> createProductManagerID(String value) {
        return new JAXBElement<String>(_ProductManagerID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseVATGroup")
    public JAXBElement<String> createPurchaseVATGroup(String value) {
        return new JAXBElement<String>(_PurchaseVATGroup_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShipType")
    public JAXBElement<String> createShipType(String value) {
        return new JAXBElement<String>(_ShipType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BrandDescription")
    public JAXBElement<String> createBrandDescription(String value) {
        return new JAXBElement<String>(_BrandDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesQtyPerPackUnit")
    public JAXBElement<String> createSalesQtyPerPackUnit(String value) {
        return new JAXBElement<String>(_SalesQtyPerPackUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VATNumber")
    public JAXBElement<String> createVATNumber(String value) {
        return new JAXBElement<String>(_VATNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DistributorID")
    public JAXBElement<String> createDistributorID(String value) {
        return new JAXBElement<String>(_DistributorID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirmCode")
    public JAXBElement<String> createFirmCode(String value) {
        return new JAXBElement<String>(_FirmCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemLivery")
    public JAXBElement<String> createItemLivery(String value) {
        return new JAXBElement<String>(_ItemLivery_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesWidthUnit")
    public JAXBElement<String> createSalesWidthUnit(String value) {
        return new JAXBElement<String>(_SalesWidthUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductManager")
    public JAXBElement<String> createProductManager(String value) {
        return new JAXBElement<String>(_ProductManager_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PostalCode")
    public JAXBElement<String> createPostalCode(String value) {
        return new JAXBElement<String>(_PostalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChannelCode")
    public JAXBElement<String> createChannelCode(String value) {
        return new JAXBElement<String>(_ChannelCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PaymentTerms")
    public JAXBElement<String> createPaymentTerms(String value) {
        return new JAXBElement<String>(_PaymentTerms_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductManagerDescription")
    public JAXBElement<String> createProductManagerDescription(String value) {
        return new JAXBElement<String>(_ProductManagerDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CommonPackDescription")
    public JAXBElement<String> createCommonPackDescription(String value) {
        return new JAXBElement<String>(_CommonPackDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GLMethod")
    public JAXBElement<String> createGLMethod(String value) {
        return new JAXBElement<String>(_GLMethod_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SafetyTime")
    public JAXBElement<String> createSafetyTime(String value) {
        return new JAXBElement<String>(_SafetyTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnitWidth")
    public JAXBElement<String> createPurchaseUnitWidth(String value) {
        return new JAXBElement<String>(_PurchaseUnitWidth_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ValuationMethod")
    public JAXBElement<String> createValuationMethod(String value) {
        return new JAXBElement<String>(_ValuationMethod_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomerItemID")
    public JAXBElement<String> createCustomerItemID(String value) {
        return new JAXBElement<String>(_CustomerItemID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EDINumber")
    public JAXBElement<String> createEDINumber(String value) {
        return new JAXBElement<String>(_EDINumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductClassification")
    public JAXBElement<String> createProductClassification(String value) {
        return new JAXBElement<String>(_ProductClassification_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TariffCode")
    public JAXBElement<String> createTariffCode(String value) {
        return new JAXBElement<String>(_TariffCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseWeightUnit")
    public JAXBElement<String> createPurchaseWeightUnit(String value) {
        return new JAXBElement<String>(_PurchaseWeightUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CommonItem")
    public JAXBElement<String> createCommonItem(String value) {
        return new JAXBElement<String>(_CommonItem_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PresentationID")
    public JAXBElement<String> createPresentationID(String value) {
        return new JAXBElement<String>(_PresentationID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomerName")
    public JAXBElement<String> createCustomerName(String value) {
        return new JAXBElement<String>(_CustomerName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountryCode")
    public JAXBElement<String> createCountryCode(String value) {
        return new JAXBElement<String>(_CountryCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesUnitWeight")
    public JAXBElement<String> createSalesUnitWeight(String value) {
        return new JAXBElement<String>(_SalesUnitWeight_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesLengthUnit")
    public JAXBElement<String> createSalesLengthUnit(String value) {
        return new JAXBElement<String>(_SalesLengthUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MinimumRemainingShelflife")
    public JAXBElement<String> createMinimumRemainingShelflife(String value) {
        return new JAXBElement<String>(_MinimumRemainingShelflife_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SignalCodeID")
    public JAXBElement<String> createSignalCodeID(String value) {
        return new JAXBElement<String>(_SignalCodeID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VATIndicator")
    public JAXBElement<String> createVATIndicator(String value) {
        return new JAXBElement<String>(_VATIndicator_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SalesItemsPerUnit")
    public JAXBElement<String> createSalesItemsPerUnit(String value) {
        return new JAXBElement<String>(_SalesItemsPerUnit_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ForecastWindow")
    public JAXBElement<String> createForecastWindow(String value) {
        return new JAXBElement<String>(_ForecastWindow_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Zone")
    public JAXBElement<String> createZone(String value) {
        return new JAXBElement<String>(_Zone_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PurchaseUnitVolume")
    public JAXBElement<String> createPurchaseUnitVolume(String value) {
        return new JAXBElement<String>(_PurchaseUnitVolume_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BlockingDays")
    public JAXBElement<String> createBlockingDays(String value) {
        return new JAXBElement<String>(_BlockingDays_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Material")
    public JAXBElement<String> createMaterial(String value) {
        return new JAXBElement<String>(_Material_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccountNumber")
    public JAXBElement<String> createAccountNumber(String value) {
        return new JAXBElement<String>(_AccountNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ControlCode")
    public JAXBElement<String> createControlCode(String value) {
        return new JAXBElement<String>(_ControlCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AreaDialing")
    public JAXBElement<String> createAreaDialing(String value) {
        return new JAXBElement<String>(_AreaDialing_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Extension")
    public JAXBElement<String> createExtension(String value) {
        return new JAXBElement<String>(_Extension_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TherapeuticAreaID")
    public JAXBElement<String> createTherapeuticAreaID(String value) {
        return new JAXBElement<String>(_TherapeuticAreaID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CreationDateTime")
    public JAXBElement<String> createCreationDateTime(String value) {
        return new JAXBElement<String>(_CreationDateTime_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ReferenceID")
    public JAXBElement<String> createReferenceID(String value) {
        return new JAXBElement<String>(_ReferenceID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SelectionCode")
    public JAXBElement<String> createSelectionCode(String value) {
        return new JAXBElement<String>(_SelectionCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShipmentDate")
    public JAXBElement<String> createShipmentDate(String value) {
        return new JAXBElement<String>(_ShipmentDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemRelationshipCode")
    public JAXBElement<String> createItemRelationshipCode(String value) {
        return new JAXBElement<String>(_ItemRelationshipCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MinimumOrderQuantity")
    public JAXBElement<String> createMinimumOrderQuantity(String value) {
        return new JAXBElement<String>(_MinimumOrderQuantity_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ParentAccountNumber")
    public JAXBElement<String> createParentAccountNumber(String value) {
        return new JAXBElement<String>(_ParentAccountNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewSiteStatisticalForecast")
    public JAXBElement<String> createViewSiteStatisticalForecast(String value) {
        return new JAXBElement<String>(_ViewSiteStatisticalForecast_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemDivision")
    public JAXBElement<String> createItemDivision(String value) {
        return new JAXBElement<String>(_ItemDivision_QNAME, String.class, null, value);
    }

}
