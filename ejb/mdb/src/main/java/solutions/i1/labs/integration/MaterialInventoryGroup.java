
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}DocumentDateTime"/>
 *         &lt;element ref="{}SiteID"/>
 *         &lt;element ref="{}BusinessUnitID"/>
 *         &lt;element ref="{}BusinessUnitDescription"/>
 *         &lt;element ref="{}RegionID"/>
 *         &lt;element ref="{}RegionDescription"/>
 *         &lt;element ref="{}CountryID"/>
 *         &lt;element ref="{}CountryDescription"/>
 *         &lt;element ref="{}DistributorID"/>
 *         &lt;element ref="{}DistributorDescription"/>
 *         &lt;element ref="{}ItemGroupID"/>
 *         &lt;element ref="{}ItemGroupDescription"/>
 *         &lt;element ref="{}BrandID"/>
 *         &lt;element ref="{}BrandDescription"/>
 *         &lt;element ref="{}ProductManagerID"/>
 *         &lt;element ref="{}ProductManagerDescription"/>
 *         &lt;element ref="{}SignalCodeID"/>
 *         &lt;element ref="{}SignalCodeDescription"/>
 *         &lt;element ref="{}PresentationID"/>
 *         &lt;element ref="{}PresentationDescription"/>
 *         &lt;element ref="{}CommonPackID"/>
 *         &lt;element ref="{}CommonPackDescription"/>
 *         &lt;element ref="{}MarketTypeID"/>
 *         &lt;element ref="{}MarketTypeDescription"/>
 *         &lt;element ref="{}WarehouseID"/>
 *         &lt;element ref="{}WarehouseDescription"/>
 *         &lt;element ref="{}TherapeuticAreaID"/>
 *         &lt;element ref="{}TherapeuticAreaDescription"/>
 *         &lt;element ref="{}FirmPeriod"/>
 *         &lt;element ref="{}MinimumOrderQuantity"/>
 *         &lt;element ref="{}IncrementalOrderQuantity"/>
 *         &lt;element ref="{}MaterialHolder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "documentDateTime",
    "siteID",
    "businessUnitID",
    "businessUnitDescription",
    "regionID",
    "regionDescription",
    "countryID",
    "countryDescription",
    "distributorID",
    "distributorDescription",
    "itemGroupID",
    "itemGroupDescription",
    "brandID",
    "brandDescription",
    "productManagerID",
    "productManagerDescription",
    "signalCodeID",
    "signalCodeDescription",
    "presentationID",
    "presentationDescription",
    "commonPackID",
    "commonPackDescription",
    "marketTypeID",
    "marketTypeDescription",
    "warehouseID",
    "warehouseDescription",
    "therapeuticAreaID",
    "therapeuticAreaDescription",
    "firmPeriod",
    "minimumOrderQuantity",
    "incrementalOrderQuantity",
    "materialHolder"
})
@XmlRootElement(name = "MaterialInventoryGroup")
public class MaterialInventoryGroup {

    @XmlElement(name = "DocumentDateTime", required = true)
    protected String documentDateTime;
    @XmlElement(name = "SiteID", required = true)
    protected String siteID;
    @XmlElement(name = "BusinessUnitID", required = true)
    protected String businessUnitID;
    @XmlElement(name = "BusinessUnitDescription", required = true)
    protected String businessUnitDescription;
    @XmlElement(name = "RegionID", required = true)
    protected String regionID;
    @XmlElement(name = "RegionDescription", required = true)
    protected String regionDescription;
    @XmlElement(name = "CountryID", required = true)
    protected String countryID;
    @XmlElement(name = "CountryDescription", required = true)
    protected String countryDescription;
    @XmlElement(name = "DistributorID", required = true)
    protected String distributorID;
    @XmlElement(name = "DistributorDescription", required = true)
    protected String distributorDescription;
    @XmlElement(name = "ItemGroupID", required = true)
    protected String itemGroupID;
    @XmlElement(name = "ItemGroupDescription", required = true)
    protected String itemGroupDescription;
    @XmlElement(name = "BrandID", required = true)
    protected String brandID;
    @XmlElement(name = "BrandDescription", required = true)
    protected String brandDescription;
    @XmlElement(name = "ProductManagerID", required = true)
    protected String productManagerID;
    @XmlElement(name = "ProductManagerDescription", required = true)
    protected String productManagerDescription;
    @XmlElement(name = "SignalCodeID", required = true)
    protected String signalCodeID;
    @XmlElement(name = "SignalCodeDescription", required = true)
    protected String signalCodeDescription;
    @XmlElement(name = "PresentationID", required = true)
    protected String presentationID;
    @XmlElement(name = "PresentationDescription", required = true)
    protected String presentationDescription;
    @XmlElement(name = "CommonPackID", required = true)
    protected String commonPackID;
    @XmlElement(name = "CommonPackDescription", required = true)
    protected String commonPackDescription;
    @XmlElement(name = "MarketTypeID", required = true)
    protected String marketTypeID;
    @XmlElement(name = "MarketTypeDescription", required = true)
    protected String marketTypeDescription;
    @XmlElement(name = "WarehouseID", required = true)
    protected String warehouseID;
    @XmlElement(name = "WarehouseDescription", required = true)
    protected String warehouseDescription;
    @XmlElement(name = "TherapeuticAreaID", required = true)
    protected String therapeuticAreaID;
    @XmlElement(name = "TherapeuticAreaDescription", required = true)
    protected String therapeuticAreaDescription;
    @XmlElement(name = "FirmPeriod", required = true)
    protected String firmPeriod;
    @XmlElement(name = "MinimumOrderQuantity", required = true)
    protected String minimumOrderQuantity;
    @XmlElement(name = "IncrementalOrderQuantity", required = true)
    protected String incrementalOrderQuantity;
    @XmlElement(name = "MaterialHolder", required = true)
    protected String materialHolder;

    /**
     * Gets the value of the documentDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDateTime() {
        return documentDateTime;
    }

    /**
     * Sets the value of the documentDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDateTime(String value) {
        this.documentDateTime = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSiteID(String value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the businessUnitID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessUnitID() {
        return businessUnitID;
    }

    /**
     * Sets the value of the businessUnitID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessUnitID(String value) {
        this.businessUnitID = value;
    }

    /**
     * Gets the value of the businessUnitDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessUnitDescription() {
        return businessUnitDescription;
    }

    /**
     * Sets the value of the businessUnitDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessUnitDescription(String value) {
        this.businessUnitDescription = value;
    }

    /**
     * Gets the value of the regionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionID() {
        return regionID;
    }

    /**
     * Sets the value of the regionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionID(String value) {
        this.regionID = value;
    }

    /**
     * Gets the value of the regionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionDescription() {
        return regionDescription;
    }

    /**
     * Sets the value of the regionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionDescription(String value) {
        this.regionDescription = value;
    }

    /**
     * Gets the value of the countryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryID() {
        return countryID;
    }

    /**
     * Sets the value of the countryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryID(String value) {
        this.countryID = value;
    }

    /**
     * Gets the value of the countryDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryDescription() {
        return countryDescription;
    }

    /**
     * Sets the value of the countryDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryDescription(String value) {
        this.countryDescription = value;
    }

    /**
     * Gets the value of the distributorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorID() {
        return distributorID;
    }

    /**
     * Sets the value of the distributorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorID(String value) {
        this.distributorID = value;
    }

    /**
     * Gets the value of the distributorDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorDescription() {
        return distributorDescription;
    }

    /**
     * Sets the value of the distributorDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorDescription(String value) {
        this.distributorDescription = value;
    }

    /**
     * Gets the value of the itemGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemGroupID() {
        return itemGroupID;
    }

    /**
     * Sets the value of the itemGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemGroupID(String value) {
        this.itemGroupID = value;
    }

    /**
     * Gets the value of the itemGroupDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemGroupDescription() {
        return itemGroupDescription;
    }

    /**
     * Sets the value of the itemGroupDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemGroupDescription(String value) {
        this.itemGroupDescription = value;
    }

    /**
     * Gets the value of the brandID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandID() {
        return brandID;
    }

    /**
     * Sets the value of the brandID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandID(String value) {
        this.brandID = value;
    }

    /**
     * Gets the value of the brandDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandDescription() {
        return brandDescription;
    }

    /**
     * Sets the value of the brandDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandDescription(String value) {
        this.brandDescription = value;
    }

    /**
     * Gets the value of the productManagerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductManagerID() {
        return productManagerID;
    }

    /**
     * Sets the value of the productManagerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductManagerID(String value) {
        this.productManagerID = value;
    }

    /**
     * Gets the value of the productManagerDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductManagerDescription() {
        return productManagerDescription;
    }

    /**
     * Sets the value of the productManagerDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductManagerDescription(String value) {
        this.productManagerDescription = value;
    }

    /**
     * Gets the value of the signalCodeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignalCodeID() {
        return signalCodeID;
    }

    /**
     * Sets the value of the signalCodeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignalCodeID(String value) {
        this.signalCodeID = value;
    }

    /**
     * Gets the value of the signalCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignalCodeDescription() {
        return signalCodeDescription;
    }

    /**
     * Sets the value of the signalCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignalCodeDescription(String value) {
        this.signalCodeDescription = value;
    }

    /**
     * Gets the value of the presentationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresentationID() {
        return presentationID;
    }

    /**
     * Sets the value of the presentationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresentationID(String value) {
        this.presentationID = value;
    }

    /**
     * Gets the value of the presentationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresentationDescription() {
        return presentationDescription;
    }

    /**
     * Sets the value of the presentationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresentationDescription(String value) {
        this.presentationDescription = value;
    }

    /**
     * Gets the value of the commonPackID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommonPackID() {
        return commonPackID;
    }

    /**
     * Sets the value of the commonPackID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommonPackID(String value) {
        this.commonPackID = value;
    }

    /**
     * Gets the value of the commonPackDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommonPackDescription() {
        return commonPackDescription;
    }

    /**
     * Sets the value of the commonPackDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommonPackDescription(String value) {
        this.commonPackDescription = value;
    }

    /**
     * Gets the value of the marketTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketTypeID() {
        return marketTypeID;
    }

    /**
     * Sets the value of the marketTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketTypeID(String value) {
        this.marketTypeID = value;
    }

    /**
     * Gets the value of the marketTypeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketTypeDescription() {
        return marketTypeDescription;
    }

    /**
     * Sets the value of the marketTypeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketTypeDescription(String value) {
        this.marketTypeDescription = value;
    }

    /**
     * Gets the value of the warehouseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarehouseID() {
        return warehouseID;
    }

    /**
     * Sets the value of the warehouseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarehouseID(String value) {
        this.warehouseID = value;
    }

    /**
     * Gets the value of the warehouseDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarehouseDescription() {
        return warehouseDescription;
    }

    /**
     * Sets the value of the warehouseDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarehouseDescription(String value) {
        this.warehouseDescription = value;
    }

    /**
     * Gets the value of the therapeuticAreaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTherapeuticAreaID() {
        return therapeuticAreaID;
    }

    /**
     * Sets the value of the therapeuticAreaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTherapeuticAreaID(String value) {
        this.therapeuticAreaID = value;
    }

    /**
     * Gets the value of the therapeuticAreaDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTherapeuticAreaDescription() {
        return therapeuticAreaDescription;
    }

    /**
     * Sets the value of the therapeuticAreaDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTherapeuticAreaDescription(String value) {
        this.therapeuticAreaDescription = value;
    }

    /**
     * Gets the value of the firmPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmPeriod() {
        return firmPeriod;
    }

    /**
     * Sets the value of the firmPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmPeriod(String value) {
        this.firmPeriod = value;
    }

    /**
     * Gets the value of the minimumOrderQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumOrderQuantity() {
        return minimumOrderQuantity;
    }

    /**
     * Sets the value of the minimumOrderQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumOrderQuantity(String value) {
        this.minimumOrderQuantity = value;
    }

    /**
     * Gets the value of the incrementalOrderQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncrementalOrderQuantity() {
        return incrementalOrderQuantity;
    }

    /**
     * Sets the value of the incrementalOrderQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncrementalOrderQuantity(String value) {
        this.incrementalOrderQuantity = value;
    }

    /**
     * Gets the value of the materialHolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaterialHolder() {
        return materialHolder;
    }

    /**
     * Sets the value of the materialHolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaterialHolder(String value) {
        this.materialHolder = value;
    }

}
