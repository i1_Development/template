
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}PartyIDs"/>
 *         &lt;element ref="{}Name"/>
 *         &lt;element ref="{}LineOfBusiness"/>
 *         &lt;element ref="{}AccountNumber"/>
 *         &lt;element ref="{}Location"/>
 *         &lt;element ref="{}Contact"/>
 *         &lt;element ref="{}AccountCurrency"/>
 *         &lt;element ref="{}AccountCategory"/>
 *         &lt;element ref="{}AccountArea"/>
 *         &lt;element ref="{}DefaultWarehouse"/>
 *         &lt;element ref="{}PaymentMethod"/>
 *       &lt;/sequence>
 *       &lt;attribute name="role" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partyIDs",
    "name",
    "lineOfBusiness",
    "accountNumber",
    "location",
    "contact",
    "accountCurrency",
    "accountCategory",
    "accountArea",
    "defaultWarehouse",
    "paymentMethod"
})
@XmlRootElement(name = "Party")
public class Party {

    @XmlElement(name = "PartyIDs", required = true)
    protected PartyIDs partyIDs;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "LineOfBusiness", required = true)
    protected String lineOfBusiness;
    @XmlElement(name = "AccountNumber", required = true)
    protected String accountNumber;
    @XmlElement(name = "Location", required = true)
    protected Location location;
    @XmlElement(name = "Contact", required = true)
    protected Contact contact;
    @XmlElement(name = "AccountCurrency", required = true)
    protected String accountCurrency;
    @XmlElement(name = "AccountCategory", required = true)
    protected String accountCategory;
    @XmlElement(name = "AccountArea", required = true)
    protected String accountArea;
    @XmlElement(name = "DefaultWarehouse", required = true)
    protected DefaultWarehouse defaultWarehouse;
    @XmlElement(name = "PaymentMethod", required = true)
    protected String paymentMethod;
    @XmlAttribute(name = "role")
    protected String role;

    /**
     * Gets the value of the partyIDs property.
     * 
     * @return
     *     possible object is
     *     {@link PartyIDs }
     *     
     */
    public PartyIDs getPartyIDs() {
        return partyIDs;
    }

    /**
     * Sets the value of the partyIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyIDs }
     *     
     */
    public void setPartyIDs(PartyIDs value) {
        this.partyIDs = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the lineOfBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineOfBusiness() {
        return lineOfBusiness;
    }

    /**
     * Sets the value of the lineOfBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineOfBusiness(String value) {
        this.lineOfBusiness = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link Location }
     *     
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link Location }
     *     
     */
    public void setLocation(Location value) {
        this.location = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContact(Contact value) {
        this.contact = value;
    }

    /**
     * Gets the value of the accountCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCurrency() {
        return accountCurrency;
    }

    /**
     * Sets the value of the accountCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCurrency(String value) {
        this.accountCurrency = value;
    }

    /**
     * Gets the value of the accountCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCategory() {
        return accountCategory;
    }

    /**
     * Sets the value of the accountCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCategory(String value) {
        this.accountCategory = value;
    }

    /**
     * Gets the value of the accountArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountArea() {
        return accountArea;
    }

    /**
     * Sets the value of the accountArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountArea(String value) {
        this.accountArea = value;
    }

    /**
     * Gets the value of the defaultWarehouse property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultWarehouse }
     *     
     */
    public DefaultWarehouse getDefaultWarehouse() {
        return defaultWarehouse;
    }

    /**
     * Sets the value of the defaultWarehouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultWarehouse }
     *     
     */
    public void setDefaultWarehouse(DefaultWarehouse value) {
        this.defaultWarehouse = value;
    }

    /**
     * Gets the value of the paymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets the value of the paymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethod(String value) {
        this.paymentMethod = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

}
