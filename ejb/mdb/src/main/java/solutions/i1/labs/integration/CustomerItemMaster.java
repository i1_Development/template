
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element ref="{}EDINumber" minOccurs="0"/>
 *           &lt;element ref="{}ApplicationArea" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element ref="{}Customers" minOccurs="0"/>
 *           &lt;element ref="{}DataArea" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element ref="{}ItemID" minOccurs="0"/>
 *         &lt;element ref="{}CustomerItemID" minOccurs="0"/>
 *         &lt;element ref="{}ManufacturerItemID" minOccurs="0"/>
 *         &lt;element ref="{}SupplierItemID" minOccurs="0"/>
 *         &lt;element ref="{}Description" minOccurs="0"/>
 *         &lt;element ref="{}ItemType" minOccurs="0"/>
 *         &lt;element ref="{}ItemGroup" minOccurs="0"/>
 *         &lt;element ref="{}Size" minOccurs="0"/>
 *         &lt;element ref="{}Weight" minOccurs="0"/>
 *         &lt;element ref="{}SignalCode" minOccurs="0"/>
 *         &lt;element ref="{}ProductType" minOccurs="0"/>
 *         &lt;element ref="{}SelectionCode" minOccurs="0"/>
 *         &lt;element ref="{}UnitOfMeasure" minOccurs="0"/>
 *         &lt;element ref="{}StorageUnitOfMeasure" minOccurs="0"/>
 *         &lt;element ref="{}LotControl" minOccurs="0"/>
 *         &lt;element ref="{}ABCCode" minOccurs="0"/>
 *         &lt;element ref="{}SafetyStockLevel" minOccurs="0"/>
 *         &lt;element ref="{}MaximumInventory" minOccurs="0"/>
 *         &lt;element ref="{}MinimumOrderQuantity" minOccurs="0"/>
 *         &lt;element ref="{}IncrementalOrderQuantity" minOccurs="0"/>
 *         &lt;element ref="{}MaximumOrderQuantity" minOccurs="0"/>
 *         &lt;element ref="{}OrderInterval" minOccurs="0"/>
 *         &lt;element ref="{}OrderLeadTime" minOccurs="0"/>
 *         &lt;element ref="{}SafetyTime" minOccurs="0"/>
 *         &lt;element ref="{}CommodityCode" minOccurs="0"/>
 *         &lt;element ref="{}ControlCode" minOccurs="0"/>
 *         &lt;element ref="{}ItemRelationshipCode" minOccurs="0"/>
 *         &lt;element ref="{}ProductManager" minOccurs="0"/>
 *         &lt;element ref="{}CommonItem" minOccurs="0"/>
 *         &lt;element ref="{}Lifecycle" minOccurs="0"/>
 *         &lt;element ref="{}Dosage" minOccurs="0"/>
 *         &lt;element ref="{}NappiCode" minOccurs="0"/>
 *         &lt;element ref="{}ProductClassification" minOccurs="0"/>
 *         &lt;element ref="{}TariffCode" minOccurs="0"/>
 *         &lt;element ref="{}SalesFocusGroup" minOccurs="0"/>
 *         &lt;element ref="{}PharmaceuticalSchedule" minOccurs="0"/>
 *         &lt;element ref="{}EAN" minOccurs="0"/>
 *         &lt;element ref="{}HarmonisedTariffCode" minOccurs="0"/>
 *         &lt;element ref="{}OverseasItem" minOccurs="0"/>
 *         &lt;element ref="{}NarcoticIngredientType" minOccurs="0"/>
 *         &lt;element ref="{}ItemDivision" minOccurs="0"/>
 *         &lt;element ref="{}DistributionFeeItemGroup" minOccurs="0"/>
 *         &lt;element ref="{}ConversionFactor" minOccurs="0"/>
 *         &lt;element ref="{}Warehouse" minOccurs="0"/>
 *         &lt;element ref="{}ShipmentDates" minOccurs="0"/>
 *         &lt;element ref="{}DefaultSupplier" minOccurs="0"/>
 *         &lt;element ref="{}ItemLivery" minOccurs="0"/>
 *         &lt;element ref="{}ForeignName" minOccurs="0"/>
 *         &lt;element ref="{}SalesVatGroup" minOccurs="0"/>
 *         &lt;element ref="{}SupplierPurchaseCode" minOccurs="0"/>
 *         &lt;element ref="{}SellItemCustomerCode" minOccurs="0"/>
 *         &lt;element ref="{}InventoryItemCode" minOccurs="0"/>
 *         &lt;element ref="{}ValuationMethod" minOccurs="0"/>
 *         &lt;element ref="{}PlanningSystem" minOccurs="0"/>
 *         &lt;element ref="{}BlockingDays" minOccurs="0"/>
 *         &lt;element ref="{}FirmCode" minOccurs="0"/>
 *         &lt;element ref="{}SalesItemsPerUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesPackagingUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesQtyPerPackUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesUnitLength" minOccurs="0"/>
 *         &lt;element ref="{}SalesLengthUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesUnitWidth" minOccurs="0"/>
 *         &lt;element ref="{}SalesWidthUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesUnitHeight" minOccurs="0"/>
 *         &lt;element ref="{}SalesHeightUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesUnitVolume" minOccurs="0"/>
 *         &lt;element ref="{}SalesVolumeUnit" minOccurs="0"/>
 *         &lt;element ref="{}SalesUnitWeight" minOccurs="0"/>
 *         &lt;element ref="{}SalesWeightUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseItemsPerUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchasePackagingUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseQtyPerPackUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnitLength" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseLengthUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnitWidth" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseWidthUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnitHeight" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseHeightUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnitVolume" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseUnitWeight" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseWeightUnit" minOccurs="0"/>
 *         &lt;element ref="{}PurchaseVATGroup" minOccurs="0"/>
 *         &lt;element ref="{}ShipType" minOccurs="0"/>
 *         &lt;element ref="{}GLMethod" minOccurs="0"/>
 *         &lt;element ref="{}ToleranceDays" minOccurs="0"/>
 *         &lt;element ref="{}Material" minOccurs="0"/>
 *         &lt;element ref="{}MarketTypeID" minOccurs="0"/>
 *         &lt;element ref="{}TherapeuticAreaID" minOccurs="0"/>
 *         &lt;element ref="{}PhysicalDimension" minOccurs="0"/>
 *         &lt;element ref="{}MinimumRemainingShelflife" minOccurs="0"/>
 *         &lt;element ref="{}PeriodSLED" minOccurs="0"/>
 *         &lt;element ref="{}PlanningTimeFence" minOccurs="0"/>
 *         &lt;element ref="{}DistributionChannel" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ediNumber",
    "applicationArea",
    "customers",
    "dataArea",
    "itemID",
    "customerItemID",
    "manufacturerItemID",
    "supplierItemID",
    "description",
    "itemType",
    "itemGroup",
    "size",
    "weight",
    "signalCode",
    "productType",
    "selectionCode",
    "unitOfMeasure",
    "storageUnitOfMeasure",
    "lotControl",
    "abcCode",
    "safetyStockLevel",
    "maximumInventory",
    "minimumOrderQuantity",
    "incrementalOrderQuantity",
    "maximumOrderQuantity",
    "orderInterval",
    "orderLeadTime",
    "safetyTime",
    "commodityCode",
    "controlCode",
    "itemRelationshipCode",
    "productManager",
    "commonItem",
    "lifecycle",
    "dosage",
    "nappiCode",
    "productClassification",
    "tariffCode",
    "salesFocusGroup",
    "pharmaceuticalSchedule",
    "ean",
    "harmonisedTariffCode",
    "overseasItem",
    "narcoticIngredientType",
    "itemDivision",
    "distributionFeeItemGroup",
    "conversionFactor",
    "warehouse",
    "shipmentDates",
    "defaultSupplier",
    "itemLivery",
    "foreignName",
    "salesVatGroup",
    "supplierPurchaseCode",
    "sellItemCustomerCode",
    "inventoryItemCode",
    "valuationMethod",
    "planningSystem",
    "blockingDays",
    "firmCode",
    "salesItemsPerUnit",
    "salesPackagingUnit",
    "salesQtyPerPackUnit",
    "salesUnitLength",
    "salesLengthUnit",
    "salesUnitWidth",
    "salesWidthUnit",
    "salesUnitHeight",
    "salesHeightUnit",
    "salesUnitVolume",
    "salesVolumeUnit",
    "salesUnitWeight",
    "salesWeightUnit",
    "purchaseUnit",
    "purchaseItemsPerUnit",
    "purchasePackagingUnit",
    "purchaseQtyPerPackUnit",
    "purchaseUnitLength",
    "purchaseLengthUnit",
    "purchaseUnitWidth",
    "purchaseWidthUnit",
    "purchaseUnitHeight",
    "purchaseHeightUnit",
    "purchaseUnitVolume",
    "purchaseUnitWeight",
    "purchaseWeightUnit",
    "purchaseVATGroup",
    "shipType",
    "glMethod",
    "toleranceDays",
    "material",
    "marketTypeID",
    "therapeuticAreaID",
    "physicalDimension",
    "minimumRemainingShelflife",
    "periodSLED",
    "planningTimeFence",
    "distributionChannel"
})
@XmlRootElement(name = "CustomerItemMaster")
public class CustomerItemMaster {

    @XmlElement(name = "EDINumber")
    protected String ediNumber;
    @XmlElement(name = "ApplicationArea")
    protected ApplicationArea applicationArea;
    @XmlElement(name = "Customers")
    protected Customers customers;
    @XmlElement(name = "DataArea")
    protected DataArea dataArea;
    @XmlElement(name = "ItemID")
    protected String itemID;
    @XmlElement(name = "CustomerItemID")
    protected String customerItemID;
    @XmlElement(name = "ManufacturerItemID")
    protected String manufacturerItemID;
    @XmlElement(name = "SupplierItemID")
    protected String supplierItemID;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "ItemType")
    protected String itemType;
    @XmlElement(name = "ItemGroup")
    protected String itemGroup;
    @XmlElement(name = "Size")
    protected String size;
    @XmlElement(name = "Weight")
    protected Weight weight;
    @XmlElement(name = "SignalCode")
    protected String signalCode;
    @XmlElement(name = "ProductType")
    protected String productType;
    @XmlElement(name = "SelectionCode")
    protected String selectionCode;
    @XmlElement(name = "UnitOfMeasure")
    protected String unitOfMeasure;
    @XmlElement(name = "StorageUnitOfMeasure")
    protected String storageUnitOfMeasure;
    @XmlElement(name = "LotControl")
    protected String lotControl;
    @XmlElement(name = "ABCCode")
    protected String abcCode;
    @XmlElement(name = "SafetyStockLevel")
    protected String safetyStockLevel;
    @XmlElement(name = "MaximumInventory")
    protected String maximumInventory;
    @XmlElement(name = "MinimumOrderQuantity")
    protected String minimumOrderQuantity;
    @XmlElement(name = "IncrementalOrderQuantity")
    protected String incrementalOrderQuantity;
    @XmlElement(name = "MaximumOrderQuantity")
    protected String maximumOrderQuantity;
    @XmlElement(name = "OrderInterval")
    protected String orderInterval;
    @XmlElement(name = "OrderLeadTime")
    protected String orderLeadTime;
    @XmlElement(name = "SafetyTime")
    protected String safetyTime;
    @XmlElement(name = "CommodityCode")
    protected String commodityCode;
    @XmlElement(name = "ControlCode")
    protected String controlCode;
    @XmlElement(name = "ItemRelationshipCode")
    protected String itemRelationshipCode;
    @XmlElement(name = "ProductManager")
    protected String productManager;
    @XmlElement(name = "CommonItem")
    protected String commonItem;
    @XmlElement(name = "Lifecycle")
    protected String lifecycle;
    @XmlElement(name = "Dosage")
    protected String dosage;
    @XmlElement(name = "NappiCode")
    protected String nappiCode;
    @XmlElement(name = "ProductClassification")
    protected String productClassification;
    @XmlElement(name = "TariffCode")
    protected String tariffCode;
    @XmlElement(name = "SalesFocusGroup")
    protected String salesFocusGroup;
    @XmlElement(name = "PharmaceuticalSchedule")
    protected String pharmaceuticalSchedule;
    @XmlElement(name = "EAN")
    protected EAN ean;
    @XmlElement(name = "HarmonisedTariffCode")
    protected String harmonisedTariffCode;
    @XmlElement(name = "OverseasItem")
    protected String overseasItem;
    @XmlElement(name = "NarcoticIngredientType")
    protected String narcoticIngredientType;
    @XmlElement(name = "ItemDivision")
    protected String itemDivision;
    @XmlElement(name = "DistributionFeeItemGroup")
    protected String distributionFeeItemGroup;
    @XmlElement(name = "ConversionFactor")
    protected ConversionFactor conversionFactor;
    @XmlElement(name = "Warehouse")
    protected Warehouse warehouse;
    @XmlElement(name = "ShipmentDates")
    protected ShipmentDates shipmentDates;
    @XmlElement(name = "DefaultSupplier")
    protected String defaultSupplier;
    @XmlElement(name = "ItemLivery")
    protected String itemLivery;
    @XmlElement(name = "ForeignName")
    protected String foreignName;
    @XmlElement(name = "SalesVatGroup")
    protected String salesVatGroup;
    @XmlElement(name = "SupplierPurchaseCode")
    protected String supplierPurchaseCode;
    @XmlElement(name = "SellItemCustomerCode")
    protected String sellItemCustomerCode;
    @XmlElement(name = "InventoryItemCode")
    protected String inventoryItemCode;
    @XmlElement(name = "ValuationMethod")
    protected String valuationMethod;
    @XmlElement(name = "PlanningSystem")
    protected String planningSystem;
    @XmlElement(name = "BlockingDays")
    protected String blockingDays;
    @XmlElement(name = "FirmCode")
    protected String firmCode;
    @XmlElement(name = "SalesItemsPerUnit")
    protected String salesItemsPerUnit;
    @XmlElement(name = "SalesPackagingUnit")
    protected String salesPackagingUnit;
    @XmlElement(name = "SalesQtyPerPackUnit")
    protected String salesQtyPerPackUnit;
    @XmlElement(name = "SalesUnitLength")
    protected String salesUnitLength;
    @XmlElement(name = "SalesLengthUnit")
    protected String salesLengthUnit;
    @XmlElement(name = "SalesUnitWidth")
    protected String salesUnitWidth;
    @XmlElement(name = "SalesWidthUnit")
    protected String salesWidthUnit;
    @XmlElement(name = "SalesUnitHeight")
    protected String salesUnitHeight;
    @XmlElement(name = "SalesHeightUnit")
    protected String salesHeightUnit;
    @XmlElement(name = "SalesUnitVolume")
    protected String salesUnitVolume;
    @XmlElement(name = "SalesVolumeUnit")
    protected String salesVolumeUnit;
    @XmlElement(name = "SalesUnitWeight")
    protected String salesUnitWeight;
    @XmlElement(name = "SalesWeightUnit")
    protected String salesWeightUnit;
    @XmlElement(name = "PurchaseUnit")
    protected String purchaseUnit;
    @XmlElement(name = "PurchaseItemsPerUnit")
    protected String purchaseItemsPerUnit;
    @XmlElement(name = "PurchasePackagingUnit")
    protected String purchasePackagingUnit;
    @XmlElement(name = "PurchaseQtyPerPackUnit")
    protected String purchaseQtyPerPackUnit;
    @XmlElement(name = "PurchaseUnitLength")
    protected String purchaseUnitLength;
    @XmlElement(name = "PurchaseLengthUnit")
    protected String purchaseLengthUnit;
    @XmlElement(name = "PurchaseUnitWidth")
    protected String purchaseUnitWidth;
    @XmlElement(name = "PurchaseWidthUnit")
    protected String purchaseWidthUnit;
    @XmlElement(name = "PurchaseUnitHeight")
    protected String purchaseUnitHeight;
    @XmlElement(name = "PurchaseHeightUnit")
    protected String purchaseHeightUnit;
    @XmlElement(name = "PurchaseUnitVolume")
    protected String purchaseUnitVolume;
    @XmlElement(name = "PurchaseUnitWeight")
    protected String purchaseUnitWeight;
    @XmlElement(name = "PurchaseWeightUnit")
    protected String purchaseWeightUnit;
    @XmlElement(name = "PurchaseVATGroup")
    protected String purchaseVATGroup;
    @XmlElement(name = "ShipType")
    protected String shipType;
    @XmlElement(name = "GLMethod")
    protected String glMethod;
    @XmlElement(name = "ToleranceDays")
    protected String toleranceDays;
    @XmlElement(name = "Material")
    protected String material;
    @XmlElement(name = "MarketTypeID")
    protected String marketTypeID;
    @XmlElement(name = "TherapeuticAreaID")
    protected String therapeuticAreaID;
    @XmlElement(name = "PhysicalDimension")
    protected PhysicalDimension physicalDimension;
    @XmlElement(name = "MinimumRemainingShelflife")
    protected String minimumRemainingShelflife;
    @XmlElement(name = "PeriodSLED")
    protected String periodSLED;
    @XmlElement(name = "PlanningTimeFence")
    protected String planningTimeFence;
    @XmlElement(name = "DistributionChannel")
    protected String distributionChannel;

    /**
     * Gets the value of the ediNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEDINumber() {
        return ediNumber;
    }

    /**
     * Sets the value of the ediNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEDINumber(String value) {
        this.ediNumber = value;
    }

    /**
     * Gets the value of the applicationArea property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationArea }
     *     
     */
    public ApplicationArea getApplicationArea() {
        return applicationArea;
    }

    /**
     * Sets the value of the applicationArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationArea }
     *     
     */
    public void setApplicationArea(ApplicationArea value) {
        this.applicationArea = value;
    }

    /**
     * Gets the value of the customers property.
     * 
     * @return
     *     possible object is
     *     {@link Customers }
     *     
     */
    public Customers getCustomers() {
        return customers;
    }

    /**
     * Sets the value of the customers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customers }
     *     
     */
    public void setCustomers(Customers value) {
        this.customers = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link DataArea }
     *     
     */
    public DataArea getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataArea }
     *     
     */
    public void setDataArea(DataArea value) {
        this.dataArea = value;
    }

    /**
     * Gets the value of the itemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Sets the value of the itemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Gets the value of the customerItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerItemID() {
        return customerItemID;
    }

    /**
     * Sets the value of the customerItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerItemID(String value) {
        this.customerItemID = value;
    }

    /**
     * Gets the value of the manufacturerItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturerItemID() {
        return manufacturerItemID;
    }

    /**
     * Sets the value of the manufacturerItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturerItemID(String value) {
        this.manufacturerItemID = value;
    }

    /**
     * Gets the value of the supplierItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierItemID() {
        return supplierItemID;
    }

    /**
     * Sets the value of the supplierItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierItemID(String value) {
        this.supplierItemID = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the itemType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the value of the itemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemType(String value) {
        this.itemType = value;
    }

    /**
     * Gets the value of the itemGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemGroup() {
        return itemGroup;
    }

    /**
     * Sets the value of the itemGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemGroup(String value) {
        this.itemGroup = value;
    }

    /**
     * Gets the value of the size property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets the value of the size property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSize(String value) {
        this.size = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link Weight }
     *     
     */
    public Weight getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Weight }
     *     
     */
    public void setWeight(Weight value) {
        this.weight = value;
    }

    /**
     * Gets the value of the signalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignalCode() {
        return signalCode;
    }

    /**
     * Sets the value of the signalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignalCode(String value) {
        this.signalCode = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the selectionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelectionCode() {
        return selectionCode;
    }

    /**
     * Sets the value of the selectionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelectionCode(String value) {
        this.selectionCode = value;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

    /**
     * Gets the value of the storageUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStorageUnitOfMeasure() {
        return storageUnitOfMeasure;
    }

    /**
     * Sets the value of the storageUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStorageUnitOfMeasure(String value) {
        this.storageUnitOfMeasure = value;
    }

    /**
     * Gets the value of the lotControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLotControl() {
        return lotControl;
    }

    /**
     * Sets the value of the lotControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLotControl(String value) {
        this.lotControl = value;
    }

    /**
     * Gets the value of the abcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getABCCode() {
        return abcCode;
    }

    /**
     * Sets the value of the abcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setABCCode(String value) {
        this.abcCode = value;
    }

    /**
     * Gets the value of the safetyStockLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSafetyStockLevel() {
        return safetyStockLevel;
    }

    /**
     * Sets the value of the safetyStockLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSafetyStockLevel(String value) {
        this.safetyStockLevel = value;
    }

    /**
     * Gets the value of the maximumInventory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumInventory() {
        return maximumInventory;
    }

    /**
     * Sets the value of the maximumInventory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumInventory(String value) {
        this.maximumInventory = value;
    }

    /**
     * Gets the value of the minimumOrderQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumOrderQuantity() {
        return minimumOrderQuantity;
    }

    /**
     * Sets the value of the minimumOrderQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumOrderQuantity(String value) {
        this.minimumOrderQuantity = value;
    }

    /**
     * Gets the value of the incrementalOrderQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncrementalOrderQuantity() {
        return incrementalOrderQuantity;
    }

    /**
     * Sets the value of the incrementalOrderQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncrementalOrderQuantity(String value) {
        this.incrementalOrderQuantity = value;
    }

    /**
     * Gets the value of the maximumOrderQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumOrderQuantity() {
        return maximumOrderQuantity;
    }

    /**
     * Sets the value of the maximumOrderQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumOrderQuantity(String value) {
        this.maximumOrderQuantity = value;
    }

    /**
     * Gets the value of the orderInterval property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderInterval() {
        return orderInterval;
    }

    /**
     * Sets the value of the orderInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderInterval(String value) {
        this.orderInterval = value;
    }

    /**
     * Gets the value of the orderLeadTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLeadTime() {
        return orderLeadTime;
    }

    /**
     * Sets the value of the orderLeadTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLeadTime(String value) {
        this.orderLeadTime = value;
    }

    /**
     * Gets the value of the safetyTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSafetyTime() {
        return safetyTime;
    }

    /**
     * Sets the value of the safetyTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSafetyTime(String value) {
        this.safetyTime = value;
    }

    /**
     * Gets the value of the commodityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * Sets the value of the commodityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityCode(String value) {
        this.commodityCode = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the itemRelationshipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemRelationshipCode() {
        return itemRelationshipCode;
    }

    /**
     * Sets the value of the itemRelationshipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemRelationshipCode(String value) {
        this.itemRelationshipCode = value;
    }

    /**
     * Gets the value of the productManager property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductManager() {
        return productManager;
    }

    /**
     * Sets the value of the productManager property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductManager(String value) {
        this.productManager = value;
    }

    /**
     * Gets the value of the commonItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommonItem() {
        return commonItem;
    }

    /**
     * Sets the value of the commonItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommonItem(String value) {
        this.commonItem = value;
    }

    /**
     * Gets the value of the lifecycle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLifecycle() {
        return lifecycle;
    }

    /**
     * Sets the value of the lifecycle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLifecycle(String value) {
        this.lifecycle = value;
    }

    /**
     * Gets the value of the dosage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * Sets the value of the dosage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDosage(String value) {
        this.dosage = value;
    }

    /**
     * Gets the value of the nappiCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNappiCode() {
        return nappiCode;
    }

    /**
     * Sets the value of the nappiCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNappiCode(String value) {
        this.nappiCode = value;
    }

    /**
     * Gets the value of the productClassification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductClassification() {
        return productClassification;
    }

    /**
     * Sets the value of the productClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductClassification(String value) {
        this.productClassification = value;
    }

    /**
     * Gets the value of the tariffCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffCode() {
        return tariffCode;
    }

    /**
     * Sets the value of the tariffCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffCode(String value) {
        this.tariffCode = value;
    }

    /**
     * Gets the value of the salesFocusGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesFocusGroup() {
        return salesFocusGroup;
    }

    /**
     * Sets the value of the salesFocusGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesFocusGroup(String value) {
        this.salesFocusGroup = value;
    }

    /**
     * Gets the value of the pharmaceuticalSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPharmaceuticalSchedule() {
        return pharmaceuticalSchedule;
    }

    /**
     * Sets the value of the pharmaceuticalSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPharmaceuticalSchedule(String value) {
        this.pharmaceuticalSchedule = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link EAN }
     *     
     */
    public EAN getEAN() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link EAN }
     *     
     */
    public void setEAN(EAN value) {
        this.ean = value;
    }

    /**
     * Gets the value of the harmonisedTariffCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHarmonisedTariffCode() {
        return harmonisedTariffCode;
    }

    /**
     * Sets the value of the harmonisedTariffCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHarmonisedTariffCode(String value) {
        this.harmonisedTariffCode = value;
    }

    /**
     * Gets the value of the overseasItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverseasItem() {
        return overseasItem;
    }

    /**
     * Sets the value of the overseasItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverseasItem(String value) {
        this.overseasItem = value;
    }

    /**
     * Gets the value of the narcoticIngredientType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarcoticIngredientType() {
        return narcoticIngredientType;
    }

    /**
     * Sets the value of the narcoticIngredientType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarcoticIngredientType(String value) {
        this.narcoticIngredientType = value;
    }

    /**
     * Gets the value of the itemDivision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDivision() {
        return itemDivision;
    }

    /**
     * Sets the value of the itemDivision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDivision(String value) {
        this.itemDivision = value;
    }

    /**
     * Gets the value of the distributionFeeItemGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionFeeItemGroup() {
        return distributionFeeItemGroup;
    }

    /**
     * Sets the value of the distributionFeeItemGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionFeeItemGroup(String value) {
        this.distributionFeeItemGroup = value;
    }

    /**
     * Gets the value of the conversionFactor property.
     * 
     * @return
     *     possible object is
     *     {@link ConversionFactor }
     *     
     */
    public ConversionFactor getConversionFactor() {
        return conversionFactor;
    }

    /**
     * Sets the value of the conversionFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConversionFactor }
     *     
     */
    public void setConversionFactor(ConversionFactor value) {
        this.conversionFactor = value;
    }

    /**
     * Gets the value of the warehouse property.
     * 
     * @return
     *     possible object is
     *     {@link Warehouse }
     *     
     */
    public Warehouse getWarehouse() {
        return warehouse;
    }

    /**
     * Sets the value of the warehouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Warehouse }
     *     
     */
    public void setWarehouse(Warehouse value) {
        this.warehouse = value;
    }

    /**
     * Gets the value of the shipmentDates property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentDates }
     *     
     */
    public ShipmentDates getShipmentDates() {
        return shipmentDates;
    }

    /**
     * Sets the value of the shipmentDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentDates }
     *     
     */
    public void setShipmentDates(ShipmentDates value) {
        this.shipmentDates = value;
    }

    /**
     * Gets the value of the defaultSupplier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultSupplier() {
        return defaultSupplier;
    }

    /**
     * Sets the value of the defaultSupplier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultSupplier(String value) {
        this.defaultSupplier = value;
    }

    /**
     * Gets the value of the itemLivery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemLivery() {
        return itemLivery;
    }

    /**
     * Sets the value of the itemLivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemLivery(String value) {
        this.itemLivery = value;
    }

    /**
     * Gets the value of the foreignName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignName() {
        return foreignName;
    }

    /**
     * Sets the value of the foreignName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignName(String value) {
        this.foreignName = value;
    }

    /**
     * Gets the value of the salesVatGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesVatGroup() {
        return salesVatGroup;
    }

    /**
     * Sets the value of the salesVatGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesVatGroup(String value) {
        this.salesVatGroup = value;
    }

    /**
     * Gets the value of the supplierPurchaseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierPurchaseCode() {
        return supplierPurchaseCode;
    }

    /**
     * Sets the value of the supplierPurchaseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierPurchaseCode(String value) {
        this.supplierPurchaseCode = value;
    }

    /**
     * Gets the value of the sellItemCustomerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellItemCustomerCode() {
        return sellItemCustomerCode;
    }

    /**
     * Sets the value of the sellItemCustomerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellItemCustomerCode(String value) {
        this.sellItemCustomerCode = value;
    }

    /**
     * Gets the value of the inventoryItemCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInventoryItemCode() {
        return inventoryItemCode;
    }

    /**
     * Sets the value of the inventoryItemCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInventoryItemCode(String value) {
        this.inventoryItemCode = value;
    }

    /**
     * Gets the value of the valuationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuationMethod() {
        return valuationMethod;
    }

    /**
     * Sets the value of the valuationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuationMethod(String value) {
        this.valuationMethod = value;
    }

    /**
     * Gets the value of the planningSystem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanningSystem() {
        return planningSystem;
    }

    /**
     * Sets the value of the planningSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanningSystem(String value) {
        this.planningSystem = value;
    }

    /**
     * Gets the value of the blockingDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlockingDays() {
        return blockingDays;
    }

    /**
     * Sets the value of the blockingDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlockingDays(String value) {
        this.blockingDays = value;
    }

    /**
     * Gets the value of the firmCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmCode() {
        return firmCode;
    }

    /**
     * Sets the value of the firmCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmCode(String value) {
        this.firmCode = value;
    }

    /**
     * Gets the value of the salesItemsPerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesItemsPerUnit() {
        return salesItemsPerUnit;
    }

    /**
     * Sets the value of the salesItemsPerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesItemsPerUnit(String value) {
        this.salesItemsPerUnit = value;
    }

    /**
     * Gets the value of the salesPackagingUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesPackagingUnit() {
        return salesPackagingUnit;
    }

    /**
     * Sets the value of the salesPackagingUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesPackagingUnit(String value) {
        this.salesPackagingUnit = value;
    }

    /**
     * Gets the value of the salesQtyPerPackUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesQtyPerPackUnit() {
        return salesQtyPerPackUnit;
    }

    /**
     * Sets the value of the salesQtyPerPackUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesQtyPerPackUnit(String value) {
        this.salesQtyPerPackUnit = value;
    }

    /**
     * Gets the value of the salesUnitLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitLength() {
        return salesUnitLength;
    }

    /**
     * Sets the value of the salesUnitLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitLength(String value) {
        this.salesUnitLength = value;
    }

    /**
     * Gets the value of the salesLengthUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesLengthUnit() {
        return salesLengthUnit;
    }

    /**
     * Sets the value of the salesLengthUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesLengthUnit(String value) {
        this.salesLengthUnit = value;
    }

    /**
     * Gets the value of the salesUnitWidth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitWidth() {
        return salesUnitWidth;
    }

    /**
     * Sets the value of the salesUnitWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitWidth(String value) {
        this.salesUnitWidth = value;
    }

    /**
     * Gets the value of the salesWidthUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesWidthUnit() {
        return salesWidthUnit;
    }

    /**
     * Sets the value of the salesWidthUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesWidthUnit(String value) {
        this.salesWidthUnit = value;
    }

    /**
     * Gets the value of the salesUnitHeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitHeight() {
        return salesUnitHeight;
    }

    /**
     * Sets the value of the salesUnitHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitHeight(String value) {
        this.salesUnitHeight = value;
    }

    /**
     * Gets the value of the salesHeightUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesHeightUnit() {
        return salesHeightUnit;
    }

    /**
     * Sets the value of the salesHeightUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesHeightUnit(String value) {
        this.salesHeightUnit = value;
    }

    /**
     * Gets the value of the salesUnitVolume property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitVolume() {
        return salesUnitVolume;
    }

    /**
     * Sets the value of the salesUnitVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitVolume(String value) {
        this.salesUnitVolume = value;
    }

    /**
     * Gets the value of the salesVolumeUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesVolumeUnit() {
        return salesVolumeUnit;
    }

    /**
     * Sets the value of the salesVolumeUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesVolumeUnit(String value) {
        this.salesVolumeUnit = value;
    }

    /**
     * Gets the value of the salesUnitWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitWeight() {
        return salesUnitWeight;
    }

    /**
     * Sets the value of the salesUnitWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitWeight(String value) {
        this.salesUnitWeight = value;
    }

    /**
     * Gets the value of the salesWeightUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesWeightUnit() {
        return salesWeightUnit;
    }

    /**
     * Sets the value of the salesWeightUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesWeightUnit(String value) {
        this.salesWeightUnit = value;
    }

    /**
     * Gets the value of the purchaseUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnit() {
        return purchaseUnit;
    }

    /**
     * Sets the value of the purchaseUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnit(String value) {
        this.purchaseUnit = value;
    }

    /**
     * Gets the value of the purchaseItemsPerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseItemsPerUnit() {
        return purchaseItemsPerUnit;
    }

    /**
     * Sets the value of the purchaseItemsPerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseItemsPerUnit(String value) {
        this.purchaseItemsPerUnit = value;
    }

    /**
     * Gets the value of the purchasePackagingUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchasePackagingUnit() {
        return purchasePackagingUnit;
    }

    /**
     * Sets the value of the purchasePackagingUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchasePackagingUnit(String value) {
        this.purchasePackagingUnit = value;
    }

    /**
     * Gets the value of the purchaseQtyPerPackUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseQtyPerPackUnit() {
        return purchaseQtyPerPackUnit;
    }

    /**
     * Sets the value of the purchaseQtyPerPackUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseQtyPerPackUnit(String value) {
        this.purchaseQtyPerPackUnit = value;
    }

    /**
     * Gets the value of the purchaseUnitLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnitLength() {
        return purchaseUnitLength;
    }

    /**
     * Sets the value of the purchaseUnitLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnitLength(String value) {
        this.purchaseUnitLength = value;
    }

    /**
     * Gets the value of the purchaseLengthUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseLengthUnit() {
        return purchaseLengthUnit;
    }

    /**
     * Sets the value of the purchaseLengthUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseLengthUnit(String value) {
        this.purchaseLengthUnit = value;
    }

    /**
     * Gets the value of the purchaseUnitWidth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnitWidth() {
        return purchaseUnitWidth;
    }

    /**
     * Sets the value of the purchaseUnitWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnitWidth(String value) {
        this.purchaseUnitWidth = value;
    }

    /**
     * Gets the value of the purchaseWidthUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseWidthUnit() {
        return purchaseWidthUnit;
    }

    /**
     * Sets the value of the purchaseWidthUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseWidthUnit(String value) {
        this.purchaseWidthUnit = value;
    }

    /**
     * Gets the value of the purchaseUnitHeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnitHeight() {
        return purchaseUnitHeight;
    }

    /**
     * Sets the value of the purchaseUnitHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnitHeight(String value) {
        this.purchaseUnitHeight = value;
    }

    /**
     * Gets the value of the purchaseHeightUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseHeightUnit() {
        return purchaseHeightUnit;
    }

    /**
     * Sets the value of the purchaseHeightUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseHeightUnit(String value) {
        this.purchaseHeightUnit = value;
    }

    /**
     * Gets the value of the purchaseUnitVolume property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnitVolume() {
        return purchaseUnitVolume;
    }

    /**
     * Sets the value of the purchaseUnitVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnitVolume(String value) {
        this.purchaseUnitVolume = value;
    }

    /**
     * Gets the value of the purchaseUnitWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseUnitWeight() {
        return purchaseUnitWeight;
    }

    /**
     * Sets the value of the purchaseUnitWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseUnitWeight(String value) {
        this.purchaseUnitWeight = value;
    }

    /**
     * Gets the value of the purchaseWeightUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseWeightUnit() {
        return purchaseWeightUnit;
    }

    /**
     * Sets the value of the purchaseWeightUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseWeightUnit(String value) {
        this.purchaseWeightUnit = value;
    }

    /**
     * Gets the value of the purchaseVATGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseVATGroup() {
        return purchaseVATGroup;
    }

    /**
     * Sets the value of the purchaseVATGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseVATGroup(String value) {
        this.purchaseVATGroup = value;
    }

    /**
     * Gets the value of the shipType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipType() {
        return shipType;
    }

    /**
     * Sets the value of the shipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipType(String value) {
        this.shipType = value;
    }

    /**
     * Gets the value of the glMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLMethod() {
        return glMethod;
    }

    /**
     * Sets the value of the glMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLMethod(String value) {
        this.glMethod = value;
    }

    /**
     * Gets the value of the toleranceDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToleranceDays() {
        return toleranceDays;
    }

    /**
     * Sets the value of the toleranceDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToleranceDays(String value) {
        this.toleranceDays = value;
    }

    /**
     * Gets the value of the material property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaterial() {
        return material;
    }

    /**
     * Sets the value of the material property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaterial(String value) {
        this.material = value;
    }

    /**
     * Gets the value of the marketTypeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketTypeID() {
        return marketTypeID;
    }

    /**
     * Sets the value of the marketTypeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketTypeID(String value) {
        this.marketTypeID = value;
    }

    /**
     * Gets the value of the therapeuticAreaID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTherapeuticAreaID() {
        return therapeuticAreaID;
    }

    /**
     * Sets the value of the therapeuticAreaID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTherapeuticAreaID(String value) {
        this.therapeuticAreaID = value;
    }

    /**
     * Gets the value of the physicalDimension property.
     * 
     * @return
     *     possible object is
     *     {@link PhysicalDimension }
     *     
     */
    public PhysicalDimension getPhysicalDimension() {
        return physicalDimension;
    }

    /**
     * Sets the value of the physicalDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link PhysicalDimension }
     *     
     */
    public void setPhysicalDimension(PhysicalDimension value) {
        this.physicalDimension = value;
    }

    /**
     * Gets the value of the minimumRemainingShelflife property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumRemainingShelflife() {
        return minimumRemainingShelflife;
    }

    /**
     * Sets the value of the minimumRemainingShelflife property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumRemainingShelflife(String value) {
        this.minimumRemainingShelflife = value;
    }

    /**
     * Gets the value of the periodSLED property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodSLED() {
        return periodSLED;
    }

    /**
     * Sets the value of the periodSLED property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodSLED(String value) {
        this.periodSLED = value;
    }

    /**
     * Gets the value of the planningTimeFence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanningTimeFence() {
        return planningTimeFence;
    }

    /**
     * Sets the value of the planningTimeFence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanningTimeFence(String value) {
        this.planningTimeFence = value;
    }

    /**
     * Gets the value of the distributionChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionChannel() {
        return distributionChannel;
    }

    /**
     * Sets the value of the distributionChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionChannel(String value) {
        this.distributionChannel = value;
    }

}
