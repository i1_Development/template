
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ChannelCode"/>
 *         &lt;element ref="{}CountryDialing"/>
 *         &lt;element ref="{}AreaDialing"/>
 *         &lt;element ref="{}DialNumber"/>
 *         &lt;element ref="{}Extension"/>
 *         &lt;element ref="{}FaxNumber"/>
 *         &lt;element ref="{}CellNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "channelCode",
    "countryDialing",
    "areaDialing",
    "dialNumber",
    "extension",
    "faxNumber",
    "cellNumber"
})
@XmlRootElement(name = "Communication")
public class Communication {

    @XmlElement(name = "ChannelCode", required = true)
    protected String channelCode;
    @XmlElement(name = "CountryDialing", required = true)
    protected String countryDialing;
    @XmlElement(name = "AreaDialing", required = true)
    protected String areaDialing;
    @XmlElement(name = "DialNumber", required = true)
    protected String dialNumber;
    @XmlElement(name = "Extension", required = true)
    protected String extension;
    @XmlElement(name = "FaxNumber", required = true)
    protected String faxNumber;
    @XmlElement(name = "CellNumber", required = true)
    protected String cellNumber;

    /**
     * Gets the value of the channelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * Sets the value of the channelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelCode(String value) {
        this.channelCode = value;
    }

    /**
     * Gets the value of the countryDialing property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryDialing() {
        return countryDialing;
    }

    /**
     * Sets the value of the countryDialing property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryDialing(String value) {
        this.countryDialing = value;
    }

    /**
     * Gets the value of the areaDialing property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaDialing() {
        return areaDialing;
    }

    /**
     * Sets the value of the areaDialing property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaDialing(String value) {
        this.areaDialing = value;
    }

    /**
     * Gets the value of the dialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDialNumber() {
        return dialNumber;
    }

    /**
     * Sets the value of the dialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDialNumber(String value) {
        this.dialNumber = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the cellNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellNumber() {
        return cellNumber;
    }

    /**
     * Sets the value of the cellNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellNumber(String value) {
        this.cellNumber = value;
    }

}
