
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}MaterialCustomer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "materialCustomer"
})
@XmlRootElement(name = "Customers")
public class Customers {

    @XmlElement(name = "MaterialCustomer", required = true)
    protected MaterialCustomer materialCustomer;

    /**
     * Gets the value of the materialCustomer property.
     * 
     * @return
     *     possible object is
     *     {@link MaterialCustomer }
     *     
     */
    public MaterialCustomer getMaterialCustomer() {
        return materialCustomer;
    }

    /**
     * Sets the value of the materialCustomer property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialCustomer }
     *     
     */
    public void setMaterialCustomer(MaterialCustomer value) {
        this.materialCustomer = value;
    }

}
