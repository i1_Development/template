
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}MaterialInventoryGroup"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "materialInventoryGroup"
})
@XmlRootElement(name = "InventoryGroup")
public class InventoryGroup {

    @XmlElement(name = "MaterialInventoryGroup", required = true)
    protected MaterialInventoryGroup materialInventoryGroup;

    /**
     * Gets the value of the materialInventoryGroup property.
     * 
     * @return
     *     possible object is
     *     {@link MaterialInventoryGroup }
     *     
     */
    public MaterialInventoryGroup getMaterialInventoryGroup() {
        return materialInventoryGroup;
    }

    /**
     * Sets the value of the materialInventoryGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialInventoryGroup }
     *     
     */
    public void setMaterialInventoryGroup(MaterialInventoryGroup value) {
        this.materialInventoryGroup = value;
    }

}
