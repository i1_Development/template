package solutions.i1.labs.mdb;

import solutions.i1.labs.integration.CustomerItemMaster;
import solutions.i1.labs.model.master.CustomersEntity;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by cola on 2017/03/22.
 */
@MessageDriven(name = "CustomerItemMasterEJB",
        activationConfig = {
            @ActivationConfigProperty(propertyName = "destination", propertyValue = "MDM"),
            @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        },
        mappedName = "MDM"
)
public class CustomerItemMasterBean implements MessageListener {

    public static Logger logger = Logger.getLogger(CustomerItemMasterBean.class.getName());

    @PersistenceContext(unitName = "RHMasterDataPU")
    EntityManager em;

    public CustomerItemMasterBean() {
    }

    @Override
    public void onMessage(Message inMessage) {
        System.out.println("Hello");
        logger.log(Level.INFO,"GOT A MESSAGE");
        TextMessage msg = null;
        Connection con = null;
        try {
            if (inMessage instanceof TextMessage) {
                msg = (TextMessage) inMessage;
                logger.log(Level.INFO,"MESSAGE BEAN: Message received: " + msg.getText());
                JAXBContext jc = JAXBContext.newInstance(CustomerItemMaster.class);
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                InputStream in = new ByteArrayInputStream(msg.getText().getBytes("UTF-8"));
                CustomerItemMaster item = (CustomerItemMaster) unmarshaller.unmarshal(in);
                logger.log(Level.INFO, "------>"+item.getDataArea().getCustomerItemMaster().getCustomers().getMaterialCustomer().getCustomerCode());

                TypedQuery query = em.createNamedQuery("CustomersEntity.findAll",CustomersEntity.class);

                logger.log(Level.INFO,""+query.getResultList().size());
            }
        }catch (Exception ex){
            logger.log(Level.SEVERE, ex.getMessage());
            ex.printStackTrace();
        }
    }
}
