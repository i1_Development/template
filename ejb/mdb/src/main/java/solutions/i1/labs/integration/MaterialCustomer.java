
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CustomerCode"/>
 *         &lt;element ref="{}EndMarketCountry"/>
 *         &lt;element ref="{}ParentAccountNumber"/>
 *         &lt;element ref="{}CustomerName"/>
 *         &lt;element ref="{}RegisteredName"/>
 *         &lt;element ref="{}PharmacyRegistrationNumber"/>
 *         &lt;element ref="{}PharmacyExpiryDate"/>
 *         &lt;element ref="{}MedPageNumber"/>
 *         &lt;element ref="{}Party"/>
 *         &lt;element ref="{}VATNumber"/>
 *         &lt;element ref="{}CreditLimit"/>
 *         &lt;element ref="{}InsuredCreditLimit"/>
 *         &lt;element ref="{}MonthEndDate"/>
 *         &lt;element ref="{}Division"/>
 *         &lt;element ref="{}Route"/>
 *         &lt;element ref="{}Insurance"/>
 *         &lt;element ref="{}EDINumber"/>
 *         &lt;element ref="{}DistributorCode"/>
 *         &lt;element ref="{}DocumentDateTime"/>
 *         &lt;element ref="{}Status"/>
 *         &lt;element ref="{}MarketSegment"/>
 *         &lt;element ref="{}CustomerLastUpdateDate"/>
 *         &lt;element ref="{}CustomerLastUpdateTime"/>
 *         &lt;element ref="{}StatementDay"/>
 *         &lt;element ref="{}ScheduleCode"/>
 *         &lt;element ref="{}ViewSiteStatisticalForecast"/>
 *         &lt;element ref="{}ForecastWindow"/>
 *         &lt;element ref="{}ForecastMandatory"/>
 *         &lt;element ref="{}BusinessPartnerType"/>
 *         &lt;element ref="{}ModeOfTransport"/>
 *         &lt;element ref="{}VATIndicator"/>
 *         &lt;element ref="{}PaymentTerms"/>
 *         &lt;element ref="{}IncoTerms"/>
 *         &lt;element ref="{}InventoryGroup"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerCode",
    "endMarketCountry",
    "parentAccountNumber",
    "customerName",
    "registeredName",
    "pharmacyRegistrationNumber",
    "pharmacyExpiryDate",
    "medPageNumber",
    "party",
    "vatNumber",
    "creditLimit",
    "insuredCreditLimit",
    "monthEndDate",
    "division",
    "route",
    "insurance",
    "ediNumber",
    "distributorCode",
    "documentDateTime",
    "status",
    "marketSegment",
    "customerLastUpdateDate",
    "customerLastUpdateTime",
    "statementDay",
    "scheduleCode",
    "viewSiteStatisticalForecast",
    "forecastWindow",
    "forecastMandatory",
    "businessPartnerType",
    "modeOfTransport",
    "vatIndicator",
    "paymentTerms",
    "incoTerms",
    "inventoryGroup"
})
@XmlRootElement(name = "MaterialCustomer")
public class MaterialCustomer {

    @XmlElement(name = "CustomerCode", required = true)
    protected String customerCode;
    @XmlElement(name = "EndMarketCountry", required = true)
    protected String endMarketCountry;
    @XmlElement(name = "ParentAccountNumber", required = true)
    protected String parentAccountNumber;
    @XmlElement(name = "CustomerName", required = true)
    protected String customerName;
    @XmlElement(name = "RegisteredName", required = true)
    protected String registeredName;
    @XmlElement(name = "PharmacyRegistrationNumber", required = true)
    protected String pharmacyRegistrationNumber;
    @XmlElement(name = "PharmacyExpiryDate", required = true)
    protected String pharmacyExpiryDate;
    @XmlElement(name = "MedPageNumber", required = true)
    protected String medPageNumber;
    @XmlElement(name = "Party", required = true)
    protected Party party;
    @XmlElement(name = "VATNumber", required = true)
    protected String vatNumber;
    @XmlElement(name = "CreditLimit", required = true)
    protected CreditLimit creditLimit;
    @XmlElement(name = "InsuredCreditLimit", required = true)
    protected InsuredCreditLimit insuredCreditLimit;
    @XmlElement(name = "MonthEndDate", required = true)
    protected String monthEndDate;
    @XmlElement(name = "Division", required = true)
    protected String division;
    @XmlElement(name = "Route", required = true)
    protected String route;
    @XmlElement(name = "Insurance", required = true)
    protected Insurance insurance;
    @XmlElement(name = "EDINumber", required = true)
    protected String ediNumber;
    @XmlElement(name = "DistributorCode", required = true)
    protected String distributorCode;
    @XmlElement(name = "DocumentDateTime", required = true)
    protected String documentDateTime;
    @XmlElement(name = "Status", required = true)
    protected Status status;
    @XmlElement(name = "MarketSegment", required = true)
    protected String marketSegment;
    @XmlElement(name = "CustomerLastUpdateDate", required = true)
    protected String customerLastUpdateDate;
    @XmlElement(name = "CustomerLastUpdateTime", required = true)
    protected String customerLastUpdateTime;
    @XmlElement(name = "StatementDay", required = true)
    protected String statementDay;
    @XmlElement(name = "ScheduleCode", required = true)
    protected String scheduleCode;
    @XmlElement(name = "ViewSiteStatisticalForecast", required = true)
    protected String viewSiteStatisticalForecast;
    @XmlElement(name = "ForecastWindow", required = true)
    protected String forecastWindow;
    @XmlElement(name = "ForecastMandatory", required = true)
    protected String forecastMandatory;
    @XmlElement(name = "BusinessPartnerType", required = true)
    protected String businessPartnerType;
    @XmlElement(name = "ModeOfTransport", required = true)
    protected String modeOfTransport;
    @XmlElement(name = "VATIndicator", required = true)
    protected String vatIndicator;
    @XmlElement(name = "PaymentTerms", required = true)
    protected String paymentTerms;
    @XmlElement(name = "IncoTerms", required = true)
    protected String incoTerms;
    @XmlElement(name = "InventoryGroup", required = true)
    protected InventoryGroup inventoryGroup;

    /**
     * Gets the value of the customerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the value of the customerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCode(String value) {
        this.customerCode = value;
    }

    /**
     * Gets the value of the endMarketCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndMarketCountry() {
        return endMarketCountry;
    }

    /**
     * Sets the value of the endMarketCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndMarketCountry(String value) {
        this.endMarketCountry = value;
    }

    /**
     * Gets the value of the parentAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentAccountNumber() {
        return parentAccountNumber;
    }

    /**
     * Sets the value of the parentAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentAccountNumber(String value) {
        this.parentAccountNumber = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the registeredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * Sets the value of the registeredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisteredName(String value) {
        this.registeredName = value;
    }

    /**
     * Gets the value of the pharmacyRegistrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPharmacyRegistrationNumber() {
        return pharmacyRegistrationNumber;
    }

    /**
     * Sets the value of the pharmacyRegistrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPharmacyRegistrationNumber(String value) {
        this.pharmacyRegistrationNumber = value;
    }

    /**
     * Gets the value of the pharmacyExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPharmacyExpiryDate() {
        return pharmacyExpiryDate;
    }

    /**
     * Sets the value of the pharmacyExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPharmacyExpiryDate(String value) {
        this.pharmacyExpiryDate = value;
    }

    /**
     * Gets the value of the medPageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedPageNumber() {
        return medPageNumber;
    }

    /**
     * Sets the value of the medPageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedPageNumber(String value) {
        this.medPageNumber = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * @return
     *     possible object is
     *     {@link Party }
     *     
     */
    public Party getParty() {
        return party;
    }

    /**
     * Sets the value of the party property.
     * 
     * @param value
     *     allowed object is
     *     {@link Party }
     *     
     */
    public void setParty(Party value) {
        this.party = value;
    }

    /**
     * Gets the value of the vatNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATNumber() {
        return vatNumber;
    }

    /**
     * Sets the value of the vatNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATNumber(String value) {
        this.vatNumber = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link CreditLimit }
     *     
     */
    public CreditLimit getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditLimit }
     *     
     */
    public void setCreditLimit(CreditLimit value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the insuredCreditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link InsuredCreditLimit }
     *     
     */
    public InsuredCreditLimit getInsuredCreditLimit() {
        return insuredCreditLimit;
    }

    /**
     * Sets the value of the insuredCreditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuredCreditLimit }
     *     
     */
    public void setInsuredCreditLimit(InsuredCreditLimit value) {
        this.insuredCreditLimit = value;
    }

    /**
     * Gets the value of the monthEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthEndDate() {
        return monthEndDate;
    }

    /**
     * Sets the value of the monthEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthEndDate(String value) {
        this.monthEndDate = value;
    }

    /**
     * Gets the value of the division property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDivision() {
        return division;
    }

    /**
     * Sets the value of the division property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDivision(String value) {
        this.division = value;
    }

    /**
     * Gets the value of the route property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets the value of the route property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoute(String value) {
        this.route = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link Insurance }
     *     
     */
    public Insurance getInsurance() {
        return insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Insurance }
     *     
     */
    public void setInsurance(Insurance value) {
        this.insurance = value;
    }

    /**
     * Gets the value of the ediNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEDINumber() {
        return ediNumber;
    }

    /**
     * Sets the value of the ediNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEDINumber(String value) {
        this.ediNumber = value;
    }

    /**
     * Gets the value of the distributorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributorCode() {
        return distributorCode;
    }

    /**
     * Sets the value of the distributorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributorCode(String value) {
        this.distributorCode = value;
    }

    /**
     * Gets the value of the documentDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDateTime() {
        return documentDateTime;
    }

    /**
     * Sets the value of the documentDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDateTime(String value) {
        this.documentDateTime = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

    /**
     * Gets the value of the marketSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketSegment() {
        return marketSegment;
    }

    /**
     * Sets the value of the marketSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketSegment(String value) {
        this.marketSegment = value;
    }

    /**
     * Gets the value of the customerLastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerLastUpdateDate() {
        return customerLastUpdateDate;
    }

    /**
     * Sets the value of the customerLastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerLastUpdateDate(String value) {
        this.customerLastUpdateDate = value;
    }

    /**
     * Gets the value of the customerLastUpdateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerLastUpdateTime() {
        return customerLastUpdateTime;
    }

    /**
     * Sets the value of the customerLastUpdateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerLastUpdateTime(String value) {
        this.customerLastUpdateTime = value;
    }

    /**
     * Gets the value of the statementDay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementDay() {
        return statementDay;
    }

    /**
     * Sets the value of the statementDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementDay(String value) {
        this.statementDay = value;
    }

    /**
     * Gets the value of the scheduleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleCode() {
        return scheduleCode;
    }

    /**
     * Sets the value of the scheduleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleCode(String value) {
        this.scheduleCode = value;
    }

    /**
     * Gets the value of the viewSiteStatisticalForecast property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewSiteStatisticalForecast() {
        return viewSiteStatisticalForecast;
    }

    /**
     * Sets the value of the viewSiteStatisticalForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewSiteStatisticalForecast(String value) {
        this.viewSiteStatisticalForecast = value;
    }

    /**
     * Gets the value of the forecastWindow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForecastWindow() {
        return forecastWindow;
    }

    /**
     * Sets the value of the forecastWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForecastWindow(String value) {
        this.forecastWindow = value;
    }

    /**
     * Gets the value of the forecastMandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForecastMandatory() {
        return forecastMandatory;
    }

    /**
     * Sets the value of the forecastMandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForecastMandatory(String value) {
        this.forecastMandatory = value;
    }

    /**
     * Gets the value of the businessPartnerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessPartnerType() {
        return businessPartnerType;
    }

    /**
     * Sets the value of the businessPartnerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessPartnerType(String value) {
        this.businessPartnerType = value;
    }

    /**
     * Gets the value of the modeOfTransport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeOfTransport() {
        return modeOfTransport;
    }

    /**
     * Sets the value of the modeOfTransport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeOfTransport(String value) {
        this.modeOfTransport = value;
    }

    /**
     * Gets the value of the vatIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATIndicator() {
        return vatIndicator;
    }

    /**
     * Sets the value of the vatIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATIndicator(String value) {
        this.vatIndicator = value;
    }

    /**
     * Gets the value of the paymentTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTerms() {
        return paymentTerms;
    }

    /**
     * Sets the value of the paymentTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTerms(String value) {
        this.paymentTerms = value;
    }

    /**
     * Gets the value of the incoTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoTerms() {
        return incoTerms;
    }

    /**
     * Sets the value of the incoTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoTerms(String value) {
        this.incoTerms = value;
    }

    /**
     * Gets the value of the inventoryGroup property.
     * 
     * @return
     *     possible object is
     *     {@link InventoryGroup }
     *     
     */
    public InventoryGroup getInventoryGroup() {
        return inventoryGroup;
    }

    /**
     * Sets the value of the inventoryGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link InventoryGroup }
     *     
     */
    public void setInventoryGroup(InventoryGroup value) {
        this.inventoryGroup = value;
    }

}
