
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}CustomerItemMaster"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerItemMaster"
})
@XmlRootElement(name = "DataArea")
public class DataArea {

    @XmlElement(name = "CustomerItemMaster", required = true)
    protected CustomerItemMaster customerItemMaster;

    /**
     * Gets the value of the customerItemMaster property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerItemMaster }
     *     
     */
    public CustomerItemMaster getCustomerItemMaster() {
        return customerItemMaster;
    }

    /**
     * Sets the value of the customerItemMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerItemMaster }
     *     
     */
    public void setCustomerItemMaster(CustomerItemMaster value) {
        this.customerItemMaster = value;
    }

}
