
package solutions.i1.labs.integration;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ConversionUnit" minOccurs="0"/>
 *         &lt;element ref="{}ConversionFactor" minOccurs="0"/>
 *         &lt;element ref="{}Length" minOccurs="0"/>
 *         &lt;element ref="{}Width" minOccurs="0"/>
 *         &lt;element ref="{}Height" minOccurs="0"/>
 *         &lt;element ref="{}Weight" minOccurs="0"/>
 *         &lt;element ref="{}TIs" minOccurs="0"/>
 *         &lt;element ref="{}HIs" minOccurs="0"/>
 *         &lt;element ref="{}EAN" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "content"
})
@XmlRootElement(name = "ConversionFactor")
public class ConversionFactor {

    @XmlElementRefs({
        @XmlElementRef(name = "ConversionUnit", type = JAXBElement.class, required = false),
        @XmlElementRef(name = "Width", type = Width.class, required = false),
        @XmlElementRef(name = "HIs", type = HIs.class, required = false),
        @XmlElementRef(name = "ConversionFactor", type = ConversionFactor.class, required = false),
        @XmlElementRef(name = "Weight", type = Weight.class, required = false),
        @XmlElementRef(name = "Length", type = Length.class, required = false),
        @XmlElementRef(name = "Height", type = Height.class, required = false),
        @XmlElementRef(name = "TIs", type = TIs.class, required = false),
        @XmlElementRef(name = "EAN", type = EAN.class, required = false)
    })
    @XmlMixed
    protected List<Object> content;

    /**
     * Gets the value of the content property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link Width }
     * {@link HIs }
     * {@link ConversionFactor }
     * {@link Weight }
     * {@link Length }
     * {@link String }
     * {@link Height }
     * {@link TIs }
     * {@link EAN }
     * 
     * 
     */
    public List<Object> getContent() {
        if (content == null) {
            content = new ArrayList<Object>();
        }
        return this.content;
    }

}
