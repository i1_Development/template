
package solutions.i1.labs.integration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}GrossWeightDimension"/>
 *         &lt;element ref="{}CubeDimension"/>
 *         &lt;element ref="{}NetWeightDimension"/>
 *         &lt;element ref="{}NetNetWeightDimension"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "grossWeightDimension",
    "cubeDimension",
    "netWeightDimension",
    "netNetWeightDimension"
})
@XmlRootElement(name = "PhysicalDimension")
public class PhysicalDimension {

    @XmlElement(name = "GrossWeightDimension", required = true)
    protected GrossWeightDimension grossWeightDimension;
    @XmlElement(name = "CubeDimension", required = true)
    protected CubeDimension cubeDimension;
    @XmlElement(name = "NetWeightDimension", required = true)
    protected NetWeightDimension netWeightDimension;
    @XmlElement(name = "NetNetWeightDimension", required = true)
    protected NetNetWeightDimension netNetWeightDimension;

    /**
     * Gets the value of the grossWeightDimension property.
     * 
     * @return
     *     possible object is
     *     {@link GrossWeightDimension }
     *     
     */
    public GrossWeightDimension getGrossWeightDimension() {
        return grossWeightDimension;
    }

    /**
     * Sets the value of the grossWeightDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link GrossWeightDimension }
     *     
     */
    public void setGrossWeightDimension(GrossWeightDimension value) {
        this.grossWeightDimension = value;
    }

    /**
     * Gets the value of the cubeDimension property.
     * 
     * @return
     *     possible object is
     *     {@link CubeDimension }
     *     
     */
    public CubeDimension getCubeDimension() {
        return cubeDimension;
    }

    /**
     * Sets the value of the cubeDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link CubeDimension }
     *     
     */
    public void setCubeDimension(CubeDimension value) {
        this.cubeDimension = value;
    }

    /**
     * Gets the value of the netWeightDimension property.
     * 
     * @return
     *     possible object is
     *     {@link NetWeightDimension }
     *     
     */
    public NetWeightDimension getNetWeightDimension() {
        return netWeightDimension;
    }

    /**
     * Sets the value of the netWeightDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetWeightDimension }
     *     
     */
    public void setNetWeightDimension(NetWeightDimension value) {
        this.netWeightDimension = value;
    }

    /**
     * Gets the value of the netNetWeightDimension property.
     * 
     * @return
     *     possible object is
     *     {@link NetNetWeightDimension }
     *     
     */
    public NetNetWeightDimension getNetNetWeightDimension() {
        return netNetWeightDimension;
    }

    /**
     * Sets the value of the netNetWeightDimension property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetNetWeightDimension }
     *     
     */
    public void setNetNetWeightDimension(NetNetWeightDimension value) {
        this.netNetWeightDimension = value;
    }

}
